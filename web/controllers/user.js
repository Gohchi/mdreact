const User = require('../models/user');

exports.get = (req, res) => {
  User.find({ name: req.query.name }, (err, users) => {
    if (err) console.log(err);
    console.log(users);
    res.send(
      `<h1>${users.length >= 1 ? users[0].name : "--"}</h1>
      <form action="/user" method="post">
        <input name="name" value="pepe" />
        <input name="lastName" value="sapo" />
        <input type="submit" />
      </form>`);
  })
};
exports.post = (req, res) => {
  var user = new User({
    name: req.body.name,
    lastName: req.body.lastName,
    fullName: `${req.body.name} ${req.body.lastName}`
  });
  user.save((err, user) => {
    if (err) return console.error(err);
  });
  res.send({ userCreatedAs: user.fullName });
};