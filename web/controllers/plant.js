const Plant = require('../models/plant');
const Silo = require('../models/silo');
const Configuration = require('../models/plant.configuration');

function getAll(customerId, setFirstAsActive){
  let promise = Plant.find({ customer: customerId })
    .select('-customer')
    .exec();
  return promise.then((plants) => {
    let list = []
    plants.forEach(plant =>
      list.push(Object.assign({
        id: plant._id,
        name: plant.name,
        active: (!setFirstAsActive ? !list.length : false), // the first,
        north: plant.north,
        longitude: -(Math.random() * (66.5 - 62) + 62),
        latitude: -(Math.random() * (37.8 - 29.5) + 29.5),
        maxTemperature: plant.maxTemperature || 50
      })))

    return list;
  });
}
exports.get = async (req, res) => {
  getAll(req.session.user.customer.id, req.query.addAll)
    .then(list => {
      Silo.find({ 'plant': { $in: list.map(o => o.id) } },
        (err, silos) => {
        if(req.query.addAll){
          list.unshift({id: "", name: "Todas las plantas", active: true, north: 315})
        }
        res.send({
          plants: list,
          silos: silos
        })
      })
      // if(req.query.config){
      //   Configuration.find({ plant: { $in: list.map(o => o.id) } }, (err, configs) => {
      //     configs.forEach(config => {
      //       let item = list.filter(i => i.id.toString() === config.plant.toString())[0];
      //       let configList = [];
      //       if(config.cnfConfig) configList.push("CNF");
      //       if(config.fcbConfig && config.fcbConfig.length) configList.push("FCB");
      //       if(config.temConfig && config.temConfig.length) configList.push("TEM");
      //       if(config.resConfig && config.resConfig.length) configList.push("RES");
      //       if(config.detConfig && config.detConfig.length) configList.push("DET");
      //       if(config.incConfig && config.incConfig.length) configList.push("INC");
      //       item.config = configList;
      //     });
      //     res.send({
      //       plants: list
      //     })
      //   })
      // }else{
      //   res.send({
      //     plants: list
      //   })
      // }
    })
};
exports.post = (req, res) => {
  var plant = new Plant({
    name: req.body.name,
    customer: req.session.user.customer.id
  });
  plant.save((err, user) => {
    if (err) return console.error(err);
  });
  if(req.body.getAll){
    getAll(req.session.user.customer.id)
      .then(list => {
        res.send({ plants: list, newPlantId: plant._id });
      })
  } else {
    res.send({ plantCreatedAs: plant.name, newPlantId: plant._id });
  }
};
