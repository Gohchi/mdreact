const Silo = require('../models/silo');

exports.delete = (req, res) => {
  Silo.deleteOne(
    { _id: req.body.id },
    (err, silo) => {
      if (err) return res.send(500, { error: err });
      return res.send({ message: "ok" });
    }
  );
}
exports.post = (req, res) => {
  let body = req.body;
  if (body.id) {
    Silo.updateOne(
      { _id: body.id },
      {
        name: body.name,
        siloNumber: body.siloNumber,
        type: body.type,
        plant: body.plant,
        rafters: body.rafters,
        x: body.x, y: body.y,
        width: body.width, height: body.height
      },
      (err, silo) => {
        if (err) return res.send(500, { error: err });
        return res.send({ siloCreatedAs: silo.name });
      }
    );
  } else {
    let silo = new Silo({
      name: body.name,
      siloNumber: body.siloNumber,
      type: body.type,
      plant: body.plant,
      rafters: body.rafters,
      x: body.x, y: body.y,
      width: body.width, height: body.height
    });
    silo.save((err, silo) => {
      if (err) return console.error(err);
    });
    res.send({ siloCreatedAs: silo.name });
  }
};

exports.updatePositions = (req, res) => {
  let { plantId, silos } = req.body;
  silos.forEach((silo, i) => {
    Silo.updateOne(
      { plant: plantId, siloNumber: silo.siloNumber },
      {
        x: silo.x,
        y: silo.y
      },
      (err, silo) => {
        if (err) return res.send(500, { error: err });
        if(i == silos.length - 1) { // the last one
          res.send({ updated: true });
        }
      }
    );
  })
  
}