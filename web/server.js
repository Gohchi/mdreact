const express = require('express');
const expressValidator =  require('express-validator');
const session =  require('express-session');
const bodyParser =  require('body-parser') ;
const cookeParser =  require('cookie-parser');
const routes = require('./routes/route');
const path = require('path');
const app = express();
const port = process.env.PORT || 5000;
const env = process.env.ENV || 'dev';

var sess = {
  key: 'user_sid',
  secret: 'keyboard cat',
  // cookie: {},
  // genid: function(req) {
  //   return genuuid() // use UUIDs for session IDs
  // },
  resave: false,
  saveUninitialized: false,
  cookie: {
      expires: 60000 * 60 * 24 // hours
  }
}

console.log('env:', env);

if (env === 'production') {
  console.log('using build');
  app.use(express.static(path.join(__dirname, 'client/build'))); // use build
  app.set('trust proxy', 1) // trust first proxy
  sess.cookie.secure = true // serve secure cookies
  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname+'/client/build/index.html'));
  });
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookeParser());

app.use(session(sess));
app.use(expressValidator());

app.use((req, res, next) => {
  if (req.cookies.user_sid && !req.session.user) {
      res.clearCookie('user_sid');
  }
  next();
});

// if(process.env.NODE_ENV === 'production') {
//   app.use(express.static('build')); //path to your build directory
//   var path = require('path');
//   app.get('*', (req, res)=> {
//     res.sendFile(path.resolve(__dirname, 'build', 'index.html'));
//   });
// }
app.set('view engine', 'pug');
app.set('views', __dirname + '/views');

app.use('/', routes);

// console.log that your server is up and running
app.listen(port, () => console.log(`Listening on port ${port}`));
