const mongoose = require('mongoose');
let schema = new mongoose.Schema({
  // id: Number,
  silo: { type: mongoose.Schema.Types.ObjectId, ref: 'Silo' },
  temperatureByTC: { type: Array, default: [] },
  dates: { type: Array, default: [] }
});
// to show ID
schema.virtual('id').get(function(){
  return this._id ? this._id.toHexString() : '';
});
schema.set('toJSON', {
  virtuals: true
});
module.exports = mongoose
  .model(
    'Temperature',
    schema
  );