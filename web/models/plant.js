const mongoose = require('mongoose');
module.exports = mongoose
  .model(
    'Plant',
    new mongoose.Schema({
      // id: Number,
      name: String,
      customer: { type: mongoose.Schema.Types.ObjectId, ref: 'Customer' },
      north: Number,
      maxTemperature: Number
    })
  );