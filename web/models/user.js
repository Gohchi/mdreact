const mongoose = require('mongoose');
module.exports = mongoose
  .model(
    'User',
    new mongoose.Schema({
      // id: Number,
      name: String,
      lastName: String,
      email: String,
      fullName: String,
      customer: { type: mongoose.Schema.Types.ObjectId, ref: 'Customer' }
    })
  );