const mongoose = require('mongoose');
let schema = new mongoose.Schema({
  // id: Number,
  name: String,
  type: String,
  plant: { type: mongoose.Schema.Types.ObjectId, ref: 'Plant' },
  x: { type: Number, default: 0 },
  y: { type: Number, default: 0 },
  width: Number,
  height: Number,
  siloNumber: Number,
  rafters: { type: Array, default: [] },
  empty: { type: Boolean, default: false },
  TcStart: { type: Number, default: 0 },
  TcEnd: { type: Number, default: 0 },
  filledLevel: { type: Number, default: 0 }
});
// to show ID
schema.virtual('id').get(function(){
  return this._id ? this._id.toHexString() : '';
});
schema.set('toJSON', {
  virtuals: true
});
module.exports = mongoose
  .model(
    'Silo',
    schema
  );