const mongoose = require('mongoose');
module.exports = mongoose
  .model(
    'Configuration',
    new mongoose.Schema({
      plant: { type: mongoose.Schema.Types.ObjectId, ref: 'Plant' },
      // id: Number,
      cnfLoaded: Boolean,
      cnfConfig: Object,
      fcbConfig: Array,
      cabConfig: Array,
      temConfig: Array,
      resConfig: Array,
      detConfig: Array,
      incConfig: Array,
      allLoaded: Boolean
    })
  );