const host = "https://test.sh";
const token = "";
const title = "Humeter";
const dbdata = "mongodb://127.0.0.1/local";
export { token, host, title, dbdata }

export const awsConfig = {
  cognito: {
      userPoolId: 'us-east-1_vVhjBmOWV', // e.g. us-east-2_uXboG5pAb
      userPoolClientId: '2pho7ttuv7lr15vmlsei086tjq', // e.g. 25ddkmj4v6hfsfvruhpfi7n4hv
      region: 'us-east-1' // e.g. us-east-2
  },
  api: {
      invokeUrl: 'https://x7yrxz7obd.execute-api.us-east-1.amazonaws.com/prod' // e.g. https://rc7nyt4tql.execute-api.us-west-2.amazonaws.com/prod,
  }
};