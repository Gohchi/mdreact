export const ADD_TODO = 'ADD_TODO'
export const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER'
export const TOGGLE_TODO = 'TOGGLE_TODO'
export const CLEAR_TODO = 'CLEAR_TODO'

// views
export const GET_VIEWS = 'GET_VIEWS'
export const ADD_VIEWS = 'ADD_VIEWS'

// user
export const SET_USER_DATA = 'SET_USER_DATA'
export const SET_LOG_IN = 'SET_LOG_IN'

// misc
export const SET_LOADING = 'SET_LOADING'
export const SET_LOGIN_ERROR = 'SET_LOGIN_ERROR'


let nextTodoId = 0
export const addTodo = (text) => {
  return {
    type: ADD_TODO,
    id: nextTodoId++,
    text
  }
}

export const setVisibilityFilter = (filter) => {
  return {
    type: SET_VISIBILITY_FILTER,
    filter
  }
}

export const toggleTodo = (id) => {
  return {
    type: TOGGLE_TODO,
    id
  }
}

export const clearTodo = () => {
  return {
    type: CLEAR_TODO
  }
}


export const views = () => {
  return {
    type: GET_VIEWS
  }
}
export const addViews = (items) => {
  return {
    type: ADD_VIEWS,
    items
  }
}

export const setUserData = (data) => {
  return {
    type: SET_USER_DATA,
    data
  }
}


export const setLogIn = (loggedIn, authToken) => {
  return {
    type: SET_LOG_IN,
    loggedIn,
    authToken
  }
}

export const setLoading = (isLoading) => {
  return {
    type: SET_LOADING,
    isLoading
  }
}
export const setLoginError = (hasError, message) => {
  return {
    type: SET_LOGIN_ERROR,
    hasError,
    message
  }
}

export const ENQUEUE_SNACKBAR = 'ENQUEUE_SNACKBAR';
export const CLOSE_SNACKBAR = 'CLOSE_SNACKBAR';
export const REMOVE_SNACKBAR = 'REMOVE_SNACKBAR';

export const enqueueSnackbar = notification => {
    const key = notification.options && notification.options.key;

    return {
        type: ENQUEUE_SNACKBAR,
        notification: {
            ...notification,
            key: key || new Date().getTime() + Math.random(),
        },
    };
};

export const closeSnackbar = key => ({
    type: CLOSE_SNACKBAR,
    dismissAll: !key, // dismiss all if no key has been defined
    key,
});

export const removeSnackbar = key => ({
    type: REMOVE_SNACKBAR,
    key,
});
