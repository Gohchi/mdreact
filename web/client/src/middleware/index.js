import { SET_LOG_IN, ADD_VIEWS } from '../actions'
// import { awsConfig } from '../config'

// const url = awsConfig.api.invokeUrl;

export const mainMiddleware = (store, action) => {
  switch (action.type){
    case SET_LOG_IN:
      if(action.loggedIn){
        // fetch(url + '/toolbar/menuitems', { 
        fetch('/toolbar/menuitems', { 
          method: 'GET', 
          headers: new Headers({
            Authorization: action.authToken
          })
        })
        .then(response => response.json())
        .then(res => {
          store.dispatch({
            type: ADD_VIEWS,
            items: res.menuItems
          })
        });
      } else {
        store.dispatch({
          type: ADD_VIEWS,
          items: []
        })
      }
      break;
    default:
  }
}