import React from 'react';

const Weather = ({ temperature=0, humidity=0, scale='ºC', classes }) => {
  return (
  <div className={classes.container}>
    <li>
      Clima actual<br />
      {temperature}<span className="gray">{scale}</span> {humidity}% <span className="gray">HR</span>
    </li>
  </div>
  )
}

export default Weather;