import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Page from './page';

const useStyles = makeStyles(() => ({
  container: {
    fontSize: '20px',
    listStyleType: 'none',
    height: '60px',
    '& li': {
      width: '160px',
      borderRadius: '10px 0 0 10px',
      backgroundColor: '#7693fc',
      color: 'white',
      float: 'left',
      height: '60px',
      padding: '2px',
      textAlign: 'center',
    },
    '& .gray': {
      color: '#eeeeee',
    }
  },
}));

export default () => {
  const data = {
    scale: "ºC",
    temperature: 28.6,
    humidity: 84
  };
  return (
    <Page
      {...data}
      classes={useStyles()}
    />
  );
}