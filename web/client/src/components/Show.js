import React from 'react'

const Show = props => {
  return (
  <div style={{
    ...props.style,
    display: props.if ? '' : 'none',
  }}>
    {props.children}
  </div>
)}

export default Show