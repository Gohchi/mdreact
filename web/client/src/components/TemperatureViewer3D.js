import React, { Component } from 'react'
// import Slider from './Slider'
// import jsonData from './data3d'
import ControlMask from './ControlMask'
import CoverButton from './CoverButton'
import './TemperatureViewer3D.css'

class TemperatureViewer3D extends Component {
  constructor(props){
    super(props)
    this.state = {
      level: 2,
      selectedIndex: 0,
      perspectiveY: 0,
      perspectiveZ: 600, //debug 0
      perspective: 1000,
      gradient: [
        [ //  0.25: "rgb(0,0,255)", 0.55: "rgb(0,255,0)", 0.85: "yellow", 1.0: "rgb(255,0,0)"
          0, [0,0,255]
        ],
        [
          55, [0,255,0]
        ],
        [
          85, [255,255,0]
        ],
        [
          100, [255,0,0]
        ]
      ]
    }
    // this.handleChange = this.handleChange.bind(this)
  }
  toggleView(len){
    let carousel = document.querySelector('.carousel');
    let cells = carousel.querySelectorAll('.carousel__cell' + len);
    for (let i=0; i < cells.length; i++ ) {
      let cell = cells[i];
      cell.querySelectorAll('circle').forEach(c =>
        c.style.fill = 'yellow'
      );
    }
  }
  move(direction){
    let { selectedIndex, perspectiveY, perspectiveZ } = this.state;
    const step = 60;
    switch(direction){
      case 'up':
      case 'down':
        perspectiveY = direction === 'up' ? perspectiveY - step : perspectiveY + step
        break;
      case 'in':
      case 'out':
        perspectiveZ = direction === 'in' ? perspectiveZ + step : perspectiveZ - step
        break;
      case 'left':
      case 'right':
        selectedIndex = direction === 'left'
        ? selectedIndex - 1 === 0 ? 0 : selectedIndex - 1
        : selectedIndex + 1;
        break;
      default:
        return;
    }
    this.setState({
      selectedIndex,
      perspectiveY,
      perspectiveZ
    })
  }
  pickHex(color1, color2, weight) {
    var p = weight;
    var w = p * 2 - 1;
    var w1 = (w/1+1) / 2;
    var w2 = 1 - w1;
    var rgb = [Math.round(color1[0] * w1 + color2[0] * w2),
        Math.round(color1[1] * w1 + color2[1] * w2),
        Math.round(color1[2] * w1 + color2[2] * w2)];

    // console.log(rgb);
    return `${rgb[0]}, ${rgb[1]}, ${rgb[2]}`;
  }
  calculateColor(value){

    const sliderWidth = 50;
    value = value * 100 / 50;

    if(value >= 100){
      return `red`;
    }
    const { gradient } = this.state; 

    let colorRange = []
    for (let index = 0; index < gradient.length; index++) {
      let perc = gradient[index][0];
        if(value <= perc) {
          // console.log(value, perc);
          colorRange = [index-1,index];
          break;
        }
    };
    
    //Get the two closest colors
    var firstcolor = gradient[colorRange[0]][1];
    var secondcolor = gradient[colorRange[1]][1];
    
    //Calculate ratio between the two closest colors
    var firstcolor_x = sliderWidth*(gradient[colorRange[0]][0]/100);
    var secondcolor_x = sliderWidth*(gradient[colorRange[1]][0]/100)-firstcolor_x;
    var slider_x = sliderWidth*(value/100)-firstcolor_x;
    var ratio = slider_x/secondcolor_x
    
    //Get the color with pickHex(thx, less.js's mix function!)
    return this.pickHex( secondcolor,firstcolor, ratio );
  }
  addCable(cable, length){
    let circles = [];
    const tcDistance = 50;
    let cy = -(tcDistance / 2);
    // const { level } = this.state;
    var val = 0;
    var colorList = [];
    for(let i = 0; i < cable.tcs.length; i++){
      val = cable.tcs[i][0];
      if(val > 100){
        val = val - 100;
      }
      let color = this.calculateColor(val)
      colorList.push(
        <radialGradient key={i} id={"grad"+i} cx="0.45" cy="0.45" r="0.6"
            fx="0.2" fy="0.2" fr="10%">
          <stop offset="10%" stopColor={`rgba(${color}, 8)`} />
          <stop offset="95%" stopColor={`rgba(${color}, .2)`} />
        </radialGradient>
      )
      // console.log('%c Value: ' + val + ' (' + (val * 100 / 50) + '%) Color: ' + color + '!', 'color: black; background: ' + color);
      cy += tcDistance;
      circles.push(<circle key={i} cx="25" cy={cy} r="15" stroke="rgb(0, 83, 181)" strokeWidth="1" fill={`url(#grad${i})`}></circle>)
    }
    return (
      <svg key={cable.name} className={"test-silo-showcase-cable carousel__cell" + length} style={{width: 52, height: cy+tcDistance / 2}}>
        <defs>
          {colorList}
        </defs>
        <g>
          <line x1="25" y1="25" x2="25" y2={cy} stroke="rgb(0, 83, 181)" strokeWidth="1"></line>
          {circles}
        </g>
      </svg>
    );
  }
  changeCarousel(cables, radius, angleRotation){
    let carousel = document.querySelector('.carousel');
    if(!carousel) return;
    let cells = carousel.querySelectorAll('.carousel__cell' + cables[0].tcs.length);
    let cellCount; // cellCount set from cells-range input value
    const { selectedIndex, perspectiveY, perspectiveZ, perspective} = this.state;
    // let cellWidth = 100 // carousel.offsetWidth;
    // let cellHeight = carousel.offsetHeight;
    let isHorizontal = true;
    let rotateFn = isHorizontal ? 'rotateY' : 'rotateX';
    // let theta;
    // let radius, theta;
    cellCount = cables.length; //cellsRange.value;
    // theta = 360 / cellCount;
    let angleShapeRotation = angleRotation * selectedIndex * -1;

    // let cellSize = isHorizontal ? cellWidth : cellHeight;
    // radius = Math.round( ( cellSize / 2) / Math.tan( Math.PI / cellCount ) );
    // radius = Math.round( ( cellSize / 2) / Math.tan( Math.PI / /*(15 - tcLength)*/ cellCount ) );
    // console.clear();
    for ( let i=0; i < cells.length; i++ ) {
      let cable = cables[i];
      let cell = cells[i];
      if ( i < cellCount ) {
        // visible cell
        cell.style.opacity = 1;
        // let cellAngle = theta * i;
        let cellAngle = cable.angle;
        // console.log(cable.name, cable.angle, radius);
        cell.style.transform = 'translateX(-25px) ' + rotateFn + '(' + cellAngle + 'deg) translateZ(' + radius + 'px)';
      } else {
        // hidden cell
        cell.style.opacity = 0;
        cell.style.transform = 'none';
      }
    }
    // if(cables.length !== 22){
    //   return;
    // }
    // perspective
    let scene = document.querySelector('.scene');
    scene.style.perspective = perspective + "px";

    rotateCarousel();
    
    function rotateCarousel() {
      // console.log(angleRotation)
      // console.log(angleShapeRotation)

      carousel.style.transform = 'translateZ(' + (-radius + perspectiveZ) + 'px) translateY(' + perspectiveY + 'px)' + 
        rotateFn + '(' + angleShapeRotation + 'deg)';
    }
  }
  updateCarousel(){
    let data = this.props.data;
    // console.log(data)
    if(!data) return; 
    var angleRotation = 0;
    data.rafters.forEach(r => {
      if(r.cables.length > 1){
        if(angleRotation === 0){
          angleRotation = r.cables[1].angle;
          // console.log(r)
        }
        this.changeCarousel(r.cables, r.radius, angleRotation)
      } else if (r.cables.length === 1){
            
        let carousel = document.querySelector('.carousel');
        if(carousel) {
          let cell = carousel.querySelector('.carousel__cell' + r.cables[0].tcs.length);
          cell.style.transform = 'translateX(-25px)';
        }
      }
    });
    // set initials
  }
  componentDidUpdate(prevProps) {
    const { selectedSilo } = this.props;
    if(selectedSilo){

      const queryParams = new URLSearchParams(window.location.search);
      if(selectedSilo.siloNumber.toString() !== queryParams.get('id')){
        this.props.selectedChanged()
      }
    }
  }
  render() {
    const { data, emptyAction } = this.props;
    if(!data) {
      setTimeout(() => emptyAction(), 100);
      return (
        <CoverButton
          message="Cargando información detallada..."
        />
      );
    }
    let list = [];
    // let buttons = [];
    data.rafters.forEach((r, l) => {
      r.cables.forEach(c => {
        list.push(this.addCable(c, c.tcs.length));
      });
      // if(r.cables.length){
      //   let len = r.cables[0].tcs.length;
      //   buttons.push(
      //     <Button key={l} onClick={() => this.toggleView(len) }>TC: {len < 10 ? '0' + len : len}</Button>
      //   );
      // }
    });
    this.updateCarousel()

    setTimeout(() =>
      this.updateCarousel()
    , 100)
    return (
      <div className="temperatureviewer3d">
        <div className="scene-frame">
          <div className="scene">
            <div className="carousel">
              {list}
            </div>
          </div>
        </div>
        <ControlMask
          left={() => this.move('left')} 
          top={() => this.move('up')}
          centerTop={() => this.move('in')}
          centerBottom={() => this.move('out')}
          bottom={() => this.move('down') }
          right={() => this.move('right')}
          labelLeft="ROTAR ANTI-RELOJ" 
          labelTop="MOVER ARRIBA"
          labelCenterTop="+ ZOOM"
          labelCenterBottom="- ZOOM"
          labelBottom="MOVER ABAJO"
          labelRight="ROTAR RELOJ"
        />
      </div>
    );
  }
}

export default TemperatureViewer3D;
