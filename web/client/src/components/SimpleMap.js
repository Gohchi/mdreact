import React, { Component } from 'react'
import GoogleMapReact from 'google-map-react'
import { fitBounds } from 'google-map-react/utils'
import { ReactComponent as SiloBg } from '../image/svg/farm-silo.svg';
import './SimpleMap.css'

const PlantButton = ({ text, onClick }) => (
  <div className="plant-button" onClick={onClick}>
    <SiloBg />
    {text}
  </div>
);
class SimpleMap extends Component {
  static defaultProps = {
    center: {lat:-32.877194547627866, lng: -65.03825138784984},
    zoom: 11
  };

  render() {
    let { plants, onClickPlant } = this.props;
    let list = []
    for(let i = 0; i < plants.length; i++){
      let plant = plants[i];
      if(plant.id !== ""){
        list.push(
          <PlantButton 
            key={i}
            lat={plant.latitude} 
            lng={plant.longitude} 
            text={plant.name}
            onClick={() => onClickPlant(plant.id)}
          />)
      }
    }
    let center = this.props.center;
    let zoom = this.props.zoom;
    if(plants.length){
      const bounds = {
        ne: {
          lat: Math.max.apply(Math, plants.map(p => (p.latitude ? p.latitude : -100))),
          lng: Math.max.apply(Math, plants.map(p => (p.longitude ? p.longitude : -100)))
        },
        sw: {
          lat: Math.min.apply(Math, plants.map(p => (p.latitude ? p.latitude : 0))),
          lng: Math.min.apply(Math, plants.map(p => (p.longitude ? p.longitude : 0)))
        }
      };
      // console.log(bounds)

      const size = {
        width: 1000, // Map width in pixels
        height: 600, // Map height in pixels
        // width: 640, // Map width in pixels
        // height: 380, // Map height in pixels
      };

      const fit = fitBounds(bounds, size);
      center = fit.center;
      zoom = fit.zoom;
      // console.log(fit)
    }
    return (
      <GoogleMapReact
        center={center}
        zoom={zoom}
      >
        {list}
      </GoogleMapReact>
    );
  }
}

export default SimpleMap