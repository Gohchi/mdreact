import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// import ReactDOM from 'react-dom';
// import { ProgressBar, Overlay, Button } from 'react-bootstrap';
import './Blueprint.css';
import './SiloMap.css';

// class CustomPopover extends Component {
//   render() {
//     const { className, style } = this.props;
//     let now = 60
//     return (
//       <div
//         className={className}
//         style={{
//           ...style,
//           position: 'absolute',
//           backgroundColor: '#68a2e7',
//           boxShadow: '0 5px 10px rgba(0, 0, 0, 0.2)',
//           border: '1px solid #CCC',
//           borderRadius: 3,
//           marginLeft: -5,
//           marginTop: 50,
//           padding: 10,
//           fontSize: 12
//         }}
//       >
//         <ul
//           className="list-unstyled"
//         >
//           <li><strong>Silo</strong></li>
//           <li>Girasol</li>
//           {/* <li><ProgressBar variant="success" now={now} label={`${now}%`} /></li> */}
//           <li>600 ton</li>
//         </ul>
//       </div>
//     );
//   }
// }
class GraphSilo extends Component {
  constructor(props) {
    super(props)
    // console.log(props)
    this.state = {
      height: null,
      width: null,
      rafters: props.rafters ? props.rafters : [
        {
          radius: 80,
          cables: []
        },
        {
          radius: 60,
          cables: Array(12)
        },
        {
          radius: 32,
          cables: Array(7)
        },
        {
          radius: 0,
          cables: [ { angle: 1 } ]
        }
      ],
      selectedCable: null
    }
    this.rotateLeft = this.rotateLeft.bind(this)
    this.rotateRight = this.rotateRight.bind(this)
    this.rotate = this.rotate.bind(this)
    this.setSelected = this.setSelected.bind(this)
  }
  rotateLeft(value = 1){
    this.rotate(value, 'left')
  }
  rotateRight(value = 1){
    this.rotate(value, 'right')
  }
  rotate(value = 1, direction){
    // const b = {
    //   ...a,          //copy everything from a
    //   user: {        //override the user property
    //      ...a.user,  //same sane: copy the everything from a.user
    //      groups: 'some changes value'  //override a.user.group
    //   }
    // }
    let rafters = [...this.state.rafters]
    const { rafterIndex, cableIndex } = this.state.selectedCable
    if(direction === 'right')
      rafters[rafterIndex].cables[cableIndex].angle += value
    else
      rafters[rafterIndex].cables[cableIndex].angle -= value
    
    this.setState({
      rafters
    })
  }
  setSelected(rafterIndex, cableIndex) {
    let selectedCable = typeof rafterIndex === 'undefined' ? null : { rafterIndex, cableIndex }
    this.setState({
      selectedCable
    })
  }
  UNSAFE_componentWillMount(){
    let { rafters, height, width } = this.state

    let updatedRafters = []
    let max = (() => {
      var total = 0
      for (let i = 0, _len = rafters.length; i < _len; i++ ) {
          if(rafters[i].cables)
            total += rafters[i].cables.length
      }
      return total
    })()
    rafters.forEach((rafter, index) => {
      if (index === 0){
        height = ((rafter.radius * 2) * 1.1)
        width = height
      }
      updatedRafters.push({
        radius: rafter.radius,
        cables: (!rafter.cables || rafter.cables.length === 0)
          ? []
          : Array.from({ length: rafter.cables.length }, (e, i) => {
            // console.log(360 / rafter.cables.length, 'rafter', rafter.radius)
            return {
              angle: 360 / rafter.cables.length * i,
              name: (max - rafter.cables.length) + i + 1
            }
          })
      })
      if(rafter.cables)
        max -= rafter.cables.length
    })

    this.setState({
      rafters: updatedRafters,
      height,
      width
    })
  }
  render() {
    let selected = this.props.selected === this.props.sid
    let bgColor = "#0053b5"
    let fillColor = selected ? "#a897ff" : "#fff3de"
    let strokeDasharray = 3
    // let props = this.props
    const { magnify } = this.props
    const { height, width, rafters, selectedCable } = this.state
    let magnificationPercentage = magnify ? (1 + magnify / 10) : 1
    let currentHeight = height * magnificationPercentage
    let currentWidth = width * magnificationPercentage
    return (
      <div className="silo-chart">
        <svg height={currentHeight} width={currentWidth}
          ref={button => {
            this.target = button;
          }}
        >
          {
            rafters.map((rafter, i) => {
              if(rafter.cables.length === 1) return <div key={i}></div>;
              const radius = rafter.radius * magnificationPercentage
              return i === 0
                ? <circle key={i} cx={currentHeight / 2} cy={currentWidth / 2} r={radius} stroke={bgColor} strokeWidth="2" fill={fillColor}
                  onClick={() => this.props.setSelected(this.props.sid) } />
                : <circle key={i} cx={currentHeight / 2} cy={currentWidth / 2} r={radius} stroke={bgColor} strokeDasharray={strokeDasharray} strokeWidth="1" fill={fillColor}
                  onClick={() => this.props.setSelected(this.props.sid) } />
            })
          }

          {rafters.map((rafter, i) => {
            if(i === 0 || rafter.cables.length === 0) return <div key={i}></div>;

            const distance = rafter.radius * magnificationPercentage
            return rafter.cables.map((cable, l) => {
              const x = Math.sin((cable.angle)*Math.PI/180) * distance + (currentHeight / 2)
              const y = -(Math.cos((cable.angle)*Math.PI/180) * distance) + (currentWidth / 2)
              // console.log('name:', cable.name, 'angle', cable.angle, 'distance', distance, 'cx', x, 'cy', y)
              const isSelected = selectedCable && selectedCable.rafterIndex === i && selectedCable.cableIndex === l
              return (
                <g key={`${i}-${l}`}>
                  <circle onClick={() => this.setSelected(i, l) } className="svg-clickable" cx={x} cy={y} r={8 * magnificationPercentage} stroke={bgColor} strokeWidth={(isSelected ? "2" : "1")} fill={(isSelected ? "white" : "#b2cef3")} />
                  <text onClick={() => this.setSelected(i, l) } className="svg-clickable" x={x} y={y} textAnchor="middle" stroke={bgColor} fontSize={10 * magnificationPercentage} strokeWidth="1px" dy=".35em">{cable.name}</text>
                </g>
              )
            })
          })}
          Sorry, your browser does not support inline SVG.
        </svg>
{/* 
        {selectedCable ?
        <div>
          <Button onClick={() => this.rotateLeft(5)}>left 5°</Button>
          <Button onClick={() => this.rotateLeft()}>left 1°</Button>
          <Button onClick={() => this.rotateRight()}>right 1°</Button>
          <Button onClick={() => this.rotateRight(5)}>right 5°</Button>
          <Button onClick={() => this.setSelected()}>close</Button>
        </div> : null}
        <Overlay
          show={true}
          placement="left"
          container={this}
          target={() => ReactDOM.findDOMNode(this.target)}
        >
          <CustomPopover />
        </Overlay> */}
      </div>
    )
  }
}
class GraphSiloDetails extends Component {
  render(){
    const currentHeight = 500
    const currentWidth = this.props.silo[0].radius * 2
    const bgColor = "#0053b5"
    const fillColor = "#fff3de"
    return (
      <div className="silo-chart-details">
        <svg height={currentHeight+10} width={currentWidth+10}>
          <rect x={5} y={5} height={500} width={currentWidth} stroke={bgColor} strokeWidth="2" fill={fillColor} />
        </svg>
      </div>
    )
  }
}
class Blueprint extends Component {
  constructor(props){
    super(props)
    this.state = {
      x: 0,
      y: 0,
      offsetX: 0,
      offsetY: 0,
      mouseDown: false
    }
    this.svgRef = React.createRef();
  }
  UNSAFE_componentWillMount(){
    // let updatedSilos = [];
    // this.state.data.silos.forEach(silo =>
    //   updatedSilos.push({...silo, rafters: this.initSiloInfo(silo)})
    // )
    // this.setState({
    //   data: {
    //     silos: updatedSilos
    //   }
    // })
  }
  initSiloInfo(data){
    let updatedRafters = []
      
    if(data.type === 'cell') {
      let raftersWidth = data.width / data.rafters.length;
      data.rafters.forEach((rafter, i) => {
        updatedRafters.push({
          x1: data.x + raftersWidth * i + raftersWidth / 2,
          y1: data.y,
          x2: data.x + raftersWidth * i + raftersWidth / 2,
          y2: data.y + data.height,
          cables: Array.from({ length: rafter.cables.length }, (e, l) => {
            return {
              distance: data.height / rafter.cables.length * l + data.height / rafter.cables.length / 2,
              name: l+1
            }
          })
        })
      })
    } else {
      let nameSequence = 1;
      data.rafters.forEach(rafter => {
        updatedRafters.push({
          radius: rafter.radius,
          cables: (!rafter.cables || rafter.cables.length === 0)
            ? []
            : Array.from({ length: rafter.cables.length }, (e, i) => {
              return {
                angle: rafter.angle + (360 / rafter.cables.length * i),
                name: nameSequence++
              }
            })
        })
      })
    }
    return updatedRafters
  }
  createSilo(silo, key){
    const { limit1, limit2 } = this.props;
    // console.clear()
    // console.log(silo)
    // return <circle key={key} cx={silo.x} cy={silo.x} r="40" stroke="green" strokeWidth="4" fill="yellow" />
    let rafters = silo.rafters
    const queryParams = new URLSearchParams(window.location.search);
    // let magnificationPercentage = queryParams.get('silo') === silo.siloNumber.toString() ? 1.2 : 1;
    const magnificationPercentage = 1
    const selected = queryParams.get('id') === silo.siloNumber.toString()
    const strokeColor = "#0053b5"
    const borderColor = "#0053b5"
    const fillColor = silo.empty ? "#a897ff" : "#fff3de"
    const strokeDasharray = 3
    // let scaling = selected && zoom ? { transform: `scale(${1 + (zoom / 10)})` } : null

    // console.log(123123123)
    // const { height, width, rafters, selectedCable } = this.state
    const onMouseEnter = e => { e.currentTarget.parentElement.appendChild(e.currentTarget); }
    return <g key={key} transform={`scale(${magnificationPercentage})`} onMouseEnter={onMouseEnter}>
      <Link to={{ search: `?id=${silo.siloNumber}`}}>
        <text x={silo.x} y={silo.y - rafters[0].radius - 5}
          textAnchor="middle"
          stroke="black"
          fontSize={10}
          strokeWidth="1px"
        >{silo.siloNumber}</text>

      {
        silo.type === 'cell'
        ? [<rect key={0} x={silo.x} y={silo.y} width={silo.width} height={silo.height} stroke={borderColor} strokeWidth={selected ? 3 : 1} fill={fillColor} />]
          .concat(
            rafters.map((rafter, i) => {
              return <line key={i+1} x1={rafter.x1} y1={rafter.y1} x2={rafter.x2} y2={rafter.y2} stroke={strokeColor} strokeDasharray={strokeDasharray} strokeWidth="1"/>
            })
          )
        : rafters.map((rafter, i) => {
          // if(rafter.cables.length === 1) return <div key={i}></div>;
          const radius = rafter.radius
          return i === 0
            ? <circle key={i} cx={silo.x} cy={silo.y} r={radius} stroke={borderColor} strokeWidth={selected ? 3 : 1} fill={fillColor} />
            : <circle key={i} cx={silo.x} cy={silo.y} r={radius} stroke={strokeColor} strokeDasharray={strokeDasharray} strokeWidth="1" fill="none" />
        })
      }
      </Link>

      {silo.type === 'cell'
        ? rafters.map((rafter, i) => {
          return rafter.cables.map((cable, l) => {
            const x = rafter.x1;
            const y = rafter.y1 + cable.distance;
            const isSelected = false //selectedCable && selectedCable.rafterIndex === i && selectedCable.cableIndex === l
            const cableFillColor = typeof cable.avgTem !== 'undefined'
              ? cable.avgTem > limit2 ? "red" : cable.avgTem > limit1 ? "yellow" : "green" 
              : "#b2cef3";
              const cableStrokeColor = typeof cable.avgTem !== 'undefined' 
                ? cable.avgTem > limit2 ? "white" : "black" 
                : strokeColor;
            return (
              <Link to={{ search: `?id=${silo.siloNumber}&cable=${cable.name}`}} key={`${i}-${l}`}>
              <g>
                <circle className="svg-clickable" cx={x} cy={y} r={8} stroke={strokeColor} strokeWidth={(isSelected ? "2" : "1")} fill={cableFillColor} />
                <text className="svg-clickable" x={x} y={y} textAnchor="middle" stroke={cableStrokeColor} fontSize={10} strokeWidth="1px" dy=".35em">{cable.name}</text>
              </g>
              </Link>
            )
          })
        })
        : rafters.map((rafter, i) => {
          if(i === 0 || rafter.cables.length === 0) return <div key={i}></div>;

          const distance = rafter.radius;
          return rafter.cables.map((cable, l) => {
            const x = Math.sin((cable.angle)*Math.PI/180) * distance + (silo.x)
            const y = -(Math.cos((cable.angle)*Math.PI/180) * distance) + (silo.y)
            // console.log('name:', cable.name, 'angle', cable.angle, 'distance', distance, 'cx', x, 'cy', y)
            const isSelected = false //selectedCable && selectedCable.rafterIndex === i && selectedCable.cableIndex === l
            const cableFillColor = typeof cable.avgTem !== 'undefined' 
              ? cable.avgTem > limit2 ? "red" : cable.avgTem > limit1 ? "yellow" : "#47ec47" 
              : "#b2cef3";
            const cableStrokeColor = typeof cable.avgTem !== 'undefined' 
              ? cable.avgTem > limit2 ? "white" : "black" 
              : strokeColor;

            return (
              <Link to={{ search: `?id=${silo.siloNumber}&cable=${cable.name}`}} key={`${i}-${l}`}>
              <g>
                <circle className="svg-clickable" cx={x} cy={y} r={8} stroke={strokeColor} strokeWidth={(isSelected ? "2" : "1")} fill={cableFillColor} />
                <text className="svg-clickable" x={x} y={y} textAnchor="middle" stroke={cableStrokeColor} fontSize={10} strokeWidth="1px" dy=".35em">{cable.name}</text>
              </g>
              </Link>
            )
          })
        })}
    </g>
  }
  render(){
    // const { x, y } = this.state;
    const {
      plantId, scaleMap, silos, silo,
      x, y, drag, startDrag, endDrag,
      summaryTem, avgOrMax, title
    } = this.props;
    let s = scaleMap;
    let ss = silos || null;
    ss = ss || [silo];

    if(ss){
      return (
        <div className="blueprint" style={{width: this.props.width, height: this.props.height}}>
          <svg
            ref={this.svgRef}
            onMouseMove={e => drag(e, this.svgRef)}
            onMouseDown={e => startDrag(e, this.svgRef)}
            onMouseUp={endDrag}
            onMouseLeave={endDrag}
          >
            <g
              className="main-group"
              style={{ transform: `scale(${s}) translateX(${x}px) translateY(${y}px)`}}
            >
              {ss.map((silo, i) => {
                let newSilo = Object.assign({}, silo);
                if(typeof summaryTem !== 'undefined' && summaryTem.data.length){
                  let temTCCounter = 0;
                  let temperatureByTC = summaryTem.data.filter(r => r.siloNumber === silo.siloNumber)[0].temperatureByTC;
                  let tems = temperatureByTC.map(t => t > 100 ? t - 100 : t);
                  newSilo.rafters.forEach(rafter => {
                    rafter.cables.forEach(cable => {
                      let TCTems = tems.slice(temTCCounter, temTCCounter + cable.tcLength);
                      if(avgOrMax === 'AVG'){
                        let avg = TCTems.length ? TCTems.reduce(function(a, b) { return a + b; }) / TCTems.length : 0; // <= AVG
                        cable.avgTem = avg; // <= AVG
                        // console.log("silo", silo.siloNumber, "- cable", cable.name, " tems(AVG):  (", Math.round(avg), ")", TCTems.join(', '))
                      } else {
                        let max = TCTems.length ? Math.max.apply(null, TCTems) : 0; // <= MAX
                        cable.avgTem = max;
                        // console.log("cable", cable.name, " tems(MAX): (", Math.round(max), ")", TCTems.join(', '))
                      }
                      temTCCounter += cable.tcLength;
                    });
                  });
                }
                return this.createSilo(newSilo, `${plantId}-${i}`)
              }
              )}
            </g>
            <text x={10} y={25}
              stroke="black"
              fontSize={30}
              strokeWidth="1px"
            >{title}</text>
          Sorry, your browser does not support inline SVG.
          </svg>
        </div>
      )
    }
    return <div></div>
  }
}
class SiloMap extends Component {
  render(){
    const { silo, selected, width, height } = this.props;
    let maxLevel = 0;
    // let list = [];
    let shape = [];
    const baseX = 50;
    const baseY = 50;
    const baseW = 170;
    const baseH = 360;
    let maxSideLevel, maxSideAngle;
    let filledPerc = 100; // inverse
    if(silo) {
      maxLevel = Math.max.apply(Math, silo.rafters.map(function(r) { return Math.max.apply(Math, r.cables.map(function(c) { return c.tcLength; })); }))
      if(silo.rafters.length > 2){
        if(silo.rafters[1].cables[0].tcLength < silo.rafters[silo.rafters.length - 1].cables[0].tcLength){
          maxSideLevel = silo.rafters[1].cables[0].tcLength;
        }
      }
      let step = baseH / maxLevel;
      let filledLevelHeight = baseY + baseH;
      for (let i = 0; i < maxLevel; i++){
        let lineStep = baseY + (baseH * 0.04) + (i * step)
        let isSelected = selected === maxLevel - i;
        if(silo.filledLevel === maxLevel - i) filledLevelHeight = lineStep;
        if(maxSideLevel === i){
          maxSideAngle = lineStep;
        }
        if(typeof maxSideAngle !== 'undefined'){
          let a = baseY + baseH - maxSideAngle;
          let b = baseW / 2;
          let d = lineStep - maxSideAngle;
          let fix = b * d / a;
          shape.push(
            <g 
              key={i}
              className="silo-level">
              <text
                x={baseX + fix - 10} y={lineStep} textAnchor="middle" stroke={"black"} fontSize={16} strokeWidth="1px" dy=".35em">{maxLevel - i}</text>
              <line
                key={i}
                x1={baseX + fix} y1={lineStep}
                x2={baseX + baseW - fix} y2={lineStep}
                stroke={isSelected ? "red" : "#0053b555"}
                // strokeDasharray={isSelected ? 0 : 3}
                strokeWidth={15}
                onMouseOver={() => this.props.changeLevel(maxLevel - i)}
              />
            </g>
          );
        } else {
          shape.push(
            <g 
              key={i}
              className="silo-level">
              <text
                x={baseX - 10} y={lineStep} textAnchor="middle" stroke={"black"} fontSize={16} dy=".35em">{maxLevel - i}</text>
              <line
                x1={baseX} y1={lineStep}
                x2={baseX + baseW} y2={lineStep}
                stroke={isSelected ? "red" : "#0053b555"}
                // strokeDasharray={isSelected ? 0 : 3}
                strokeWidth={15}
                onMouseOver={() => this.props.changeLevel(maxLevel - i)}
              />
            </g>
          );
        }
      }
      filledPerc = filledLevelHeight * 100 / (baseY + baseH);
    }
    // console.log(maxSideAngle)
    let path = [
      `M ${baseX} ${baseY}`, // set cursor
      `L ${baseW / 2 + baseX} ${baseY - 40}`, // top cone
      `L ${baseX + baseW} ${baseY}` // to right side 
    ];
    if(typeof maxSideAngle !== 'undefined'){
      path = path.concat([
        `L ${baseX + baseW} ${maxSideAngle}`, // to bottom
        `L ${baseW / 2 + baseX} ${baseY + baseH}`, // bottom cone
        `L ${baseX} ${maxSideAngle}`, // to left
        'z' // closes the shape
      ]);
    } else {
      path = path.concat([
        `L ${baseX + baseW} ${baseY + baseH}`, // to bottom
        `L ${baseX} ${baseY + baseH}`, // to left
        'z' // closes the shape
      ]);
    }

    return (
      <div style={{width: width, height: height}}>
        <svg className="silomap">
          <g className="shape">
            <defs>
              <linearGradient id="silo-level-filled" x1="0%" y1="0%" x2="0%" y2="100%">
                <stop offset="0%" stopColor="#fff3de" stopOpacity="1" />
                <stop offset={((filledPerc - 1) + "%")} stopColor="#fff3de" stopOpacity="1" />
                <stop offset={filledPerc + "%"} stopColor="#ffb938" stopOpacity="1" />
                <stop offset="100%" stopColor="#ffb938" stopOpacity="1" />
              </linearGradient>
            </defs>
            <path 
              fill="url(#silo-level-filled)"
              // fill="#fff3de" // #8580ff
              stroke="#0053b5"
              d={path.join(' ')}
            />
            {shape}
          </g>
          Sorry, your browser does not support inline SVG.
        </svg>
      </div>
    )
  }
}
export {
  Blueprint,
  SiloMap,
  GraphSilo,
  GraphSiloDetails
}