import React from 'react';

function Page(props){
  const { date, classes } = props
  return (
    <div className={classes.container}>
      <li>
        <div>{date}</div>
      </li>
    </div>
  )
}

export default Page