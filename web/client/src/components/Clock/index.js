import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Page from './page';

const useStyles = makeStyles(() => ({
  container: {
    fontSize: '20px',
    listStyleType: 'none',
    height: '60px',
    '& li': {
      width: '120px',
      borderRadius: '0 10px 10px 0',
      backgroundColor: '#6982dd',
      color: 'white',
      float: 'left',
      height: '60px',
      padding: '2px',
      textAlign: 'center',
      // cursor: 'pointer',
    }
  },
}));
function getDateAsString(){
  let datetime = new Date()
  let date = datetime.toLocaleDateString()
  let time = datetime.toLocaleTimeString()
  time = time.split(':')
  return `${date} ${time[0]}:${time[1]}`
}
function Clock(){
  const [date, setDate] = React.useState(getDateAsString());

  function currentTime(){
    setDate(getDateAsString())
  }
  React.useEffect(() => {
    var timerID = setInterval( () => currentTime(), 1000 );

    return function cleanup() {
      clearInterval(timerID);
    };
  });
  return (
    <Page
      date={date}
      classes={useStyles()}
    />
  );
}

export default Clock;