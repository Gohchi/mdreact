import React from 'react'
import './Slider.css'

const Slider = props => (
  <input
    className={"slider" + (props.orient ? ' ' + props.orient : ' standard')}
    type="range"
    min={props.min ? props.min : 1}
    max={props.max}
    value={props.value}
    onChange={event => {
      let value = parseInt(event.target.value);
      props.handler(value);
    }}
  >
  </input>
)

export default Slider;