import React, { Component } from 'react';
import compass from '../image/svg/compass.svg';

class Compass extends Component {
  render() {
    return (
      <div
        style={{
          position: 'absolute',
          margin: '10px',
          zIndex: '100',
          cursor: 'help',
          bottom: '15px'
        }}
      >
        <img
          style={{
            width: '40px',
            height: '40px',
            transform: `rotateZ(${this.props.angle}deg)`
          }}
          className="icon compass"
          alt="Norte" title="Norte (rojo)"
          src={compass} />
      </div>
    )
  }
}

export default Compass;