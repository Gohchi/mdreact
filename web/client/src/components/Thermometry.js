// import '../tools/heatmap/heatmap.min';
import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';
// import { Link } from 'react-router-dom';
import { title } from '../config';
import { Blueprint } from './Graphs';
import Emoji from './Emoji';
import TemperatureViewer2D from './TemperatureViewer2D'
import TemperatureViewer3D from './TemperatureViewer3D'
import Compass from './Compass'
import SimpleMap from './SimpleMap'
import Show from './Show'
import Toolbar from '@material-ui/core/Toolbar'
// import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import ButtonGroup from '@material-ui/core/ButtonGroup'
// import MenuIcon from '@material-ui/icons/Menu'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown'
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';

import HistogramTC from './HistogramTC'

import './Thermometry.css';
import 'react-input-range/lib/css/index.css'

// import { silos } from '../temp/silos' 
class Thermometry extends Component {
  constructor(props){
    super(props)
    this.state = {
      leftPanelHidden: false,
      scaleMap: 1,
      // selectedSilo: 1,
      plants: [],
      silos: null,
      view3D: true,
      editMode: false,
      editModeSteps: 10,
      silosHaveChanges: false,

      realTemperature: null,
      realTemperatureDates: null,
      testTime: 1,
      currentSiloMaxLevel: null,
      activeLevel: 1,
      testSlider: 10,
      selectedSilo: null,
      showHistory: false,
      heatMapData: null,
      siloTem: null,

      siloMapX: 0,
      siloMapY: 0,
      siloMapXMouseDown: false,
      siloMapOffsetX: 0,
      siloMapOffsetY: 0,

      avgOrMax: 'AVG',
      summaryTem: null,
      menuPlantsOpen: false,
      menuAvgOrMaxOpen: false,
      menu3DOpen: false
    }
    this.menuPlantsRef = React.createRef()
    this.menuAvgOrMax = React.createRef()
    this.menu3D = React.createRef()
    this.blueprintSVG = React.createRef()
    this.blueprintG = React.createRef()

    this.handleChange = this.handleChange.bind(this)
    this.scaleMap = this.scaleMap.bind(this)
    this.setEditMode = this.setEditMode.bind(this)
    this.moveSilo = this.moveSilo.bind(this)
    this.changeSteps = this.changeSteps.bind(this)
    this.saveChanges = this.saveChanges.bind(this)
    this.checkHistory = this.checkHistory.bind(this)
    this.testHeatMap = this.testHeatMap.bind(this)
    this.selectedChanged = this.selectedChanged.bind(this)
    
    this.drag = this.drag.bind(this)
    this.startDrag = this.startDrag.bind(this)
    this.endDrag = this.endDrag.bind(this)
    this.getMousePosition = this.getMousePosition.bind(this)
  }
  resetScale(){
    let blueprint = this.blueprintSVG.current;
    let svg = blueprint.svgRef.current;
    let g = svg.children[0].getBBox();
    let w = svg.clientWidth / g.width;
    let h = svg.clientHeight / g.height;
    let scale = w < h ? w : h;

    this.setState({
      siloMapX: Math.abs(g.x),
      siloMapY: Math.abs(g.y),
      siloMapOffsetX: 0,
      siloMapOffsetY: 0,
      scaleMap: scale
    })
  }
  calculateDistance(elem, mouseX, mouseY) {
      return Math.floor(Math.sqrt(Math.pow(mouseX - (elem.offset().left+(elem.width()/2)), 2) + Math.pow(mouseY - (elem.offset().top+(elem.height()/2)), 2)));
  }
  drag(e, svgRef){
    if(this.state.siloMapXMouseDown){
      e.preventDefault();
      var coord = this.getMousePosition(e, svgRef);
      
      this.setState({
        siloMapX: coord.x - this.state.siloMapOffsetX,
        siloMapY: coord.y - this.state.siloMapOffsetY
      })
    }
  }
  getMousePosition(e, svgRef) {
    let CTM = svgRef.current.getScreenCTM();
    return {
      x: (e.clientX - CTM.e) / CTM.a,
      y: (e.clientY - CTM.f) / CTM.d
    };
  }
  startDrag(e, svgRef) {
    let offset = this.getMousePosition(e, svgRef);
    offset.x -= parseFloat(this.state.siloMapX);
    offset.y -= parseFloat(this.state.siloMapY);
    this.setState({
      siloMapXMouseDown: true,
      siloMapOffsetX: offset.x,
      siloMapOffsetY: offset.y
    })
  }
  endDrag() {
    this.setState({siloMapXMouseDown: false})
  }
  selectedChanged(){
    this.heatmapInstance = null;
    this.setState({
      selectedSilo: null,
      siloTem: null,
      activeLevel: 1
    })
  }
  asParams(params){
    const esc = encodeURIComponent;
    const query = Object.keys(params)
        .map(k => esc(k) + '=' + esc(params[k]))
        .join('&');
    return "?" + query;
  }
  getTems = async (plantId, silo) => {
    const params = this.asParams({
      "plantId": plantId,
      "silo": silo,
      "fix": true
    });
    const response = await fetch('/silo/get/temperature' + params, {
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
      }
    })
    if (response.status === 401) {
      // this.props.openLogin();
    }
    const body = await response.json()
    if (response.status !== 200) {
      console.error("something went wrong...");
    }
    // console.log(body);
    return body;
  }
  getPlantSummaryTem = async (plantId) => {
    const params = this.asParams({
      "plantId": plantId
    });
    const response = await fetch('/silo/get/temperature/summary' + params, {
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
      }
    })
    if (response.status === 401) {
      // this.props.openLogin();
    }
    const body = await response.json()
    if (response.status !== 200) {
      console.error("something went wrong...");
    }
    return body;
  }
  checkHistory(){
    this.setState({
      showHistory: !this.state.showHistory
    })
  }
  getSelectedSilo(){
    const queryParams = new URLSearchParams(window.location.search);
    if(!queryParams.get('id')) return null;
    const { plants, silos } = this.state;
    // let updatedSilos = silos.map(silo => silo)
    
    const selectedPlants = plants.map(o => o).filter(plant => plant.active);
    if (selectedPlants.length){
      const plantId = selectedPlants[0].id;
      let selectedSilos = silos.map(o => o).filter(silo => silo.plant === plantId && silo.siloNumber.toString() === queryParams.get('id'));
      if (selectedSilos.length) {
        return selectedSilos[0];
      }
    }
    return null;
  }
  getActivePlant(){
    const { plants } = this.state;
    return plants.map(o => o).filter(plant => plant.active)[0].id;
  }
  testHeatMap(newLevel, newTime){
    const queryParams = new URLSearchParams(window.location.search);
    if(!queryParams.get('id')) return;

    const { realTemperature, activeLevel, testTime, siloTem, currentSiloMaxLevel, plants } = this.state;
    let currentLevel = newLevel ? newLevel : activeLevel;
    let currentTime = newTime ? newTime : testTime;
    // const selected = queryParams.get('id') === silo.siloNumber.toString()
    const silo = this.getSelectedSilo();
    if (silo){
      // const cableId = queryParams.get('cable')
      // if(cableId === '1'){
        if(!siloTem){
          this.getTems(this.getActivePlant(), queryParams.get('id'))
            .then(body => {
              this.setState({
                siloTem: body.siloTems,
                realTemperature: body.temperatureByTC,
                realTemperatureDates: body.dates,
                selectedSilo: silo
              })
              this.testHeatMap()
            })
          return;
        }
      // }

      let siloMaxLevel = currentSiloMaxLevel == null 
          ? Math.max.apply(Math, silo.rafters.map(function(r) { return Math.max.apply(Math, r.cables.map(function(c) { return c.tcLength; })); }))
          : currentSiloMaxLevel
      let h337 = window.h337;
      let heatMapDiv = document.querySelector('.heatmap');
      // console.log(123123123123);
      if(!heatMapDiv) return;
      this.heatmapInstance = this.heatmapInstance || h337.create({
        container: heatMapDiv,
        // blur: 0.1,
        // gradient: {
        //   // enter n keys between 0 and 1 here
        //   // for gradient color customization
        //   '.5': 'yellowgreen',
        //   '.8': 'magenta',
        //   '.95': 'violet'
        // },
      });
      // console.log(heatMapDiv);

      var points = [];
      // var max = 0;
      let maxValue = 150;
      let i = 0;
      let tcIndex = siloMaxLevel - currentLevel;
      if (tcIndex < 0) tcIndex = 0;
      let baseDistance; // = 130;
      // const maxRadius = 85;
      // const baseRadius = 70;
      const maxRadius = 150;
      const baseRadius = 60;
      let currentMaxRadius;
      let currentRadius;
      if(!siloTem){
        silo.rafters.forEach(rafter => {
          let distance = rafter.radius;
          // console.log(distance);
          if(!currentMaxRadius) {
            currentMaxRadius = distance;
            // set radius to points to "zoom it"
            currentRadius = baseRadius*((1-distance/maxRadius)+1);
          }
          distance = maxRadius*(distance*100/currentMaxRadius)/100;
          if(!baseDistance) baseDistance = distance + 15;
          rafter.cables.forEach(cable => {
            const x = Math.sin((cable.angle)*Math.PI/180) * distance + baseDistance
            const y = -(Math.cos((cable.angle)*Math.PI/180) * distance) + baseDistance
            // console.log('x:',x,'y:',y)
            let val;
            if(realTemperature){
              if(tcIndex < cable.tcLength){
                val = realTemperature[i + tcIndex][37 - currentTime];
                // console.log(37 - currentTime);

                if(val > 100){
                  val = val - 100
                }
                i += cable.tcLength;
              } else {
                val = 0
              }
            } else {
              // maxValue = Math.floor(Math.random()*maxValue);
              val = Math.floor(Math.random()*maxValue);
              // val = Math.floor(Math.random()*(maxValue ? maxValue : 1));
            }
            // max = Math.max(max, val);
            // console.log(cable.name, val)
            // val = Math.floor((Math.random(0.5)+0.5)*100);
            // val = 100
            points.push({
              x: x,
              y: y,
              value: val,
              radius: currentRadius
            });
          });
        })
      } else {
        siloTem.rafters.forEach(rafter => {
          let distance = rafter.radius;
          // console.log(distance);
          if(!currentMaxRadius) {
            distance -= 5;
            currentMaxRadius = distance;
            // set radius to points to "zoom it"
            currentRadius = baseRadius*((1-distance/maxRadius)+1);
          }
          distance = maxRadius*(distance*100/currentMaxRadius)/100;
          if(!baseDistance) baseDistance = distance + 30;
          rafter.cables.forEach(cable => {
            const x = Math.sin((cable.angle)*Math.PI/180) * distance + baseDistance
            const y = -(Math.cos((cable.angle)*Math.PI/180) * distance) + baseDistance
            // console.log('x:',x,'y:',y)
            let val;
          
            if(tcIndex < cable.tcs.length){
              val = cable.tcs[tcIndex][currentTime-1];
              if(val > 100){
                val = val - 100
              }
            } else {
              val = 0
            }
            // max = Math.max(max, val);
            // console.log(cable.name, val)
            // val = Math.floor((Math.random(0.5)+0.5)*100);
            // val = 100
            points.push({
              x: x,
              y: y,
              value: val,
              radius: currentRadius
            });
          });
        })
      }
      // console.log('Level:', currentLevel, 'Values:', points.map((o, i) => (i+1) + ': '+ o.value).join(', '));
      // console.log('Level:', currentLevel);
      // points.forEach((o, i) => {
      //   if(o.value > 0) {
      //     console.log((i+1) + ': '+ o.value);
      //   }
      // });
      // console.log(i);
      // console.log(Math.max.apply(Math, points.map(function(o) { return o.value; })));
      // heatmap data format
      const selectedPlants = plants.filter(plant => plant.active);
      var data = { 
        max: selectedPlants.length ? selectedPlants[0].maxTemperature : 40,
        data: points 
      };
      // this.heatmapInstance.setData(data);

      // let heatMapCanvas = document.querySelectorAll('.heatmap-canvas')[1];
      // console.log(silo.rafters)
      // // let radius = silo.rafters[0].radius;
      // const radius = maxRadius;
      // this.clearCircle(radius+15, radius+15, radius, heatMapCanvas.getContext('2d'));
      this.setState({
        activeLevel: currentLevel,
        testTime: currentTime,
        heatMapData: {
          data: data, radius: maxRadius + 15
        },
        // testTime: (this.state.testTime < 35 ? this.state.testTime + 1 : 0),
        currentSiloMaxLevel: siloMaxLevel
      })
    }
  }
  clearCircle(x, y, radius, context)
  {
    context.save();
    context.globalCompositeOperation = 'destination-in';
    context.beginPath();
    context.arc(x, y, radius, 0, 2 * Math.PI, false);
    context.fill();
    context.scale(2, 2)
    context.restore();
    context.beginPath();
    context.strokeStyle = "#0053b5";
    context.lineWidth = 3;
    context.arc(x, y, radius, 0, 2 * Math.PI, false);
    context.stroke()
  }
  saveChanges(){
    const { plants, silos } = this.state;
    const plantId = plants.filter(plant => plant.active)[0].id;
    let updatedSilos = silos.filter(silo => silo.plant === plantId)
      .map(silo => {
        return { siloNumber: silo.siloNumber, x: silo.x, y: silo.y }
      });

    fetch('/silos/positions', {
      method: 'post',
      headers: {
          'Accept': 'text/html',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "plantId": plantId,
        "silos": updatedSilos
      })
    }).then(response => response.json())
    .then(res => {
      if (res.updated){
        this.setState({
          silosHaveChanges: false
        })
      }
    });
  }
  changeSteps(up){
    let { editModeSteps } = this.state
    
    if (editModeSteps <= 1 && !up)
      return;

    this.setState({
      editModeSteps: up ? editModeSteps + 1 : editModeSteps - 1
    })
  }
  moveSilo(direction){
    const queryParams = new URLSearchParams(window.location.search);
    if(!queryParams.get('id')) return;

    const { plants, silos, editModeSteps } = this.state;
    // const selected = queryParams.get('id') === silo.siloNumber.toString()
    const selectedPlants = plants.filter(plant => plant.active);
    if (selectedPlants.length){
      const plantId = selectedPlants[0].id;
      let updatedSilos = silos.map(silo => silo)
      let selectedSilos = updatedSilos.filter(silo => silo.plant === plantId && silo.siloNumber.toString() === queryParams.get('id'));
      if(selectedSilos.length){
        let selectedSilo = selectedSilos[0];
        switch (direction){
          case 'left':
            selectedSilo.x -= editModeSteps;
            
            break;
          case 'right':
            selectedSilo.x += editModeSteps;
            break;
          case 'down':
            selectedSilo.y += editModeSteps;
            break;
          case 'up':
            selectedSilo.y -= editModeSteps;
            break;
          default:
            return;
        }
        this.setState({
          silos: updatedSilos,
          silosHaveChanges: true
        })
      }
    }
  }
  setEditMode(){
    this.setState({
      editMode: !this.state.editMode
    })
  }
  scaleMap(value){
    // console.log(value);
    let amount = value
      ? this.state.scaleMap < 1 ? this.state.scaleMap+0.1 : this.state.scaleMap+1
      : this.state.scaleMap-1 < 1 ? this.state.scaleMap-0.1 : this.state.scaleMap-1 
    
    // console.log(amount);
    if(typeof value === 'number') amount = value
    this.setState({
      scaleMap: amount < 0.1 ? 0.1 : amount
    })
  }
  handleChange(date, prop){
    let state = prop === "start" ? { startDate: date } : { endDate: date };
    this.setState(state);
  }
  hideLeftPanel(){
    this.setState({
      leftPanelHidden: true
    })
  }
  // setSelectedSilo(sid = 0){
  //   this.setState({
  //     selectedSilo: sid
  //   })
  // }
  call = async (uri) => {
    const response = await fetch(uri);
    if (response.status === 401) {
      // this.props.openLogin();
    }
    const body = await response.json();

    // if (response.status !== 200) {
    //   throw Error(body.message) 
    // }
    return body;
  }
  getSilos(plantId){
    this.call('/silos', { plantId: plantId })
      .then(res => {
        if (!!res.isNotLoggedIn) {
          // fail          
        } else {
          this.setState({
            silos: res.silos
          })
        }
      })
      .catch(err => console.error(err));
  }
  getPlants(){
    this.call('/plants?addAll=true')
      .then(res => {
        if (!!res.isNotLoggedIn) {
          // fail          
        } else {
          // console.log(res.plants)
          this.setState({
            plants: res.plants,
            silos: res.silos
          })
          // debug
          setTimeout(() => {
            this.changePlant("5d0ec6ef65f1f342d013d830")
            // this.changePlant("5d0ec9db65f1f342d013d841") //5cae2fd5588aef0b48f811bc")
            setTimeout(() => {
              this.testHeatMap();
            }, 500)
          }, 100)
        }
      })
      .catch(err => console.error(err));
  }
  plantIsSelected(){
    const { plants } = this.state;
    const selectedPlants = plants.filter(plant => plant.active);
    if (selectedPlants.length){
      let plant = selectedPlants[0];
      if(plant.id === "") return true;
    }
    return false;
  }
  UNSAFE_componentWillMount(){
    this.getPlants()
    // this.getSilos()
  }
  UNSAFE_componentWillReceiveProps(props) {
    const { refresh } = this.props
    if (props.refresh !== refresh) {
      this.getPlants()
      // this.getSilos()
    }
  }
  changePlant(plantId){
    if(plantId === ""){
      this.setState({
        plants: this.state.plants.map(o => Object.assign({ ...o }, { active: o.id === plantId })),
        siloMapX: 0,
        siloMapY: 0,
        siloMapOffsetX: 0,
        siloMapOffsetY: 0,
        scaleMap: 1,
        summaryTem: null
      })
      this.resetScale();
    } else {
      this.getPlantSummaryTem(plantId)
        .then(body => {
          this.setState({
            plants: this.state.plants.map(o => Object.assign({ ...o }, { active: o.id === plantId })),
            siloMapX: 0,
            siloMapY: 0,
            siloMapOffsetX: 0,
            siloMapOffsetY: 0,
            scaleMap: 1,
            summaryTem: body
          })
          this.resetScale();
        })
    }
  }
  updateHeatMap(){
    const { heatMapData } = this.state

    const heatMapCanvas = document.querySelectorAll('.heatmap-canvas')[1];
    if(this.heatmapInstance && heatMapCanvas && heatMapData) {
      const { data, radius } = heatMapData
      // console.log(data);
      this.heatmapInstance.setData(data);

      const fix = 15;
      this.clearCircle(radius+fix, radius+fix, radius, heatMapCanvas.getContext('2d'));
    }
  }
  anySiloSelected(silos){
    let selected = false;
    const queryParams = new URLSearchParams(window.location.search);
    let i = 0;
    while (!selected && i < silos.length){
      const silo = silos[i];
      i++;
      selected = queryParams.get('id') === silo.siloNumber.toString();
    }
    return selected;
  }
  render() {
    const {
      plants, silos, editModeSteps, showHistory, realTemperature, realTemperatureDates, siloTem, currentSiloMaxLevel, activeLevel, selectedSilo,
      menuPlantsOpen, menuAvgOrMaxOpen, menu3DOpen
    } = this.state;
    let selectedSilos = [];
    const selectedPlants = plants.filter(plant => plant.active);
    let plantId, plant, blueprintTitle = "";
    if (selectedPlants.length){
      plant = selectedPlants[0];
      plantId = plant.id;
      blueprintTitle = plant.name;
      selectedSilos = silos.filter(silo => silo.plant === plantId);
    }
    let currentDate = "";
    if (realTemperatureDates){
      // console.log(realTemperatureDates.length);
      let date = realTemperatureDates[this.state.testTime - 1];
      currentDate =  `${date.substring(0, 10)} ${date.substring(11, 16)}`
    }
    
    // relate temperature to tcs
    // 1176 x 616
    // 1043.8 x 582.4

    // 1.0576923076923076923076923076923
    // 1.126652615443571565433991186051

    let plantIsSelected = this.plantIsSelected()
    this.updateHeatMap()
    const anySiloSelected = this.anySiloSelected(selectedSilos);

    return (
      <DocumentTitle title={`${title} - Termometría`}>
      <div className="view thermometry" style={{backgroundColor: "#87abdb"}}>
        
        <Toolbar>
          {/* <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
          >
            <MenuIcon />
          </IconButton> */}
          
          <ButtonGroup 
            variant="contained"
            color="primary"
          >
            <Button
              variant="contained"
              // size="small"
              ref={(ref) => { this.menuPlantsRef = ref}}
              aria-owns={menuPlantsOpen ? 'menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={() => this.setState({ menuPlantsOpen: !menuPlantsOpen })}
            >
              Plantas
              <ArrowDropDownIcon />
            </Button>
          </ButtonGroup>
            
          <Popper open={menuPlantsOpen} anchorEl={this.menuPlantsRef} transition disablePortal style={{ zIndex: 1 }}>
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                style={{
                  transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
                }}
              >
                <Paper id="menu-list-grow">
                  <ClickAwayListener onClickAway={() => this.setState({ menuPlantsOpen: false} )}>
                    <MenuList>
                      {plants.map((plant, i) =>
                        <MenuItem
                          key={i} 
                          onClick={() => {this.changePlant(plant.id); this.setState({ menuPlantsOpen: false} )}}
                          selected={plant.active}
                        >
                          {plant.name}
                        </MenuItem>
                      )}
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Popper>
          <ButtonGroup 
            variant="contained"
            className="buttons-zoom" title="Zoom">
            <Button onClick={() => this.scaleMap(false) }><Emoji symbol="➖" label="collapse" /></Button>
            <Button onClick={() => this.scaleMap(true) }><Emoji symbol="➕" label="collapse" /></Button>
            <Button onClick={() => this.scaleMap(0.5) }>Minimizar</Button>
            <Button onClick={() => this.scaleMap(2) }>Maximizar</Button>
            <Button
              // disabled={!(this.state.siloMapX !== 0 || this.state.siloMapY !== 0 || this.state.scaleMap !== 0)}
              onClick={() => this.resetScale()}
            >Reestablecer
            </Button>
          </ButtonGroup>
          
          <ButtonGroup 
            variant="contained"
            color="primary"
          >
            <Button
              variant="contained"
              ref={(ref) => { this.menuAvgOrMax = ref}}
              aria-owns={menuAvgOrMaxOpen ? 'menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={() => this.setState({ menuAvgOrMaxOpen: !menuAvgOrMaxOpen })}
            >
              {"Modo de resumen: " + this.state.avgOrMax}
              <ArrowDropDownIcon />
            </Button>
          </ButtonGroup>
            
          <Popper open={menuAvgOrMaxOpen} anchorEl={this.menuAvgOrMax} transition disablePortal style={{ zIndex: 1 }}>
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                style={{
                  transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
                }}
              >
                <Paper id="menu-list-grow">
                  <ClickAwayListener onClickAway={() => this.setState({ menuAvgOrMaxOpen: false} )}>
                    <MenuList>
                      <MenuItem onClick={() => this.setState({ avgOrMax: 'AVG', menuAvgOrMaxOpen: false }) }>AVG</MenuItem>
                      <MenuItem onClick={() => this.setState({ avgOrMax: 'MAX', menuAvgOrMaxOpen: false }) }>MAX</MenuItem>
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Popper>
          <Button onClick={() => this.setState({editMode: !this.state.editMode}) }>Editar</Button>
          <ButtonGroup 
            variant="contained"
            color="primary"
            style={{ marginLeft: 'auto'}}
          >
            <Button
              variant="contained"
              ref={(ref) => { this.menu3D = ref}}
              aria-owns={menu3DOpen ? 'menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={() => this.setState({ menu3DOpen: !menu3DOpen })}
            >
              {"Visualización " + (this.state.view3D ? "3D" : "2D")}
              <ArrowDropDownIcon />
            </Button>
          </ButtonGroup>
            
          <Popper open={menu3DOpen} anchorEl={this.menu3D} transition disablePortal style={{ zIndex: 1 }}>
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                style={{
                  transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
                }}
              >
                <Paper id="menu-list-grow">
                  <ClickAwayListener onClickAway={() => this.setState({ menu3DOpen: false} )}>
                    <MenuList>
                      <MenuItem onClick={() => this.setState({ view3D: true, menu3DOpen: false }) }>3D</MenuItem>
                      <MenuItem onClick={() => this.setState({ view3D: false, menu3DOpen: false }) }>2D</MenuItem>
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Popper>
          {/* <Button variant="danger" onClick={() => this.testHeatMap() }>Test heat map</Button> */}
          <Button onClick={() => this.checkHistory() }>DEBUG: Check history</Button>
        </Toolbar>
        
        <div 
          style={{
            display: this.state.editMode ? undefined : 'none',
            position: "absolute",
            zIndex: 10,
            margin: 10,
            animation: 'fadein 1s'
          }}
        >
          <ButtonGroup 
            variant="contained"
            className="buttons-zoom" title="Modifica el silo seleccionado">
            <Button onClick={() => this.moveSilo("left") }><Emoji symbol="⬅" label="collapse" /></Button>
            <Button onClick={() => this.moveSilo("right") }><Emoji symbol="➡" label="collapse" /></Button>
            <Button onClick={() => this.moveSilo("down") }><Emoji symbol="⬇" label="collapse" /></Button>
            <Button onClick={() => this.moveSilo("up") }><Emoji symbol="⬆" label="collapse" /></Button>
          </ButtonGroup>

          <ButtonGroup>
            <Button onClick={() => this.changeSteps(false) }><Emoji symbol="➖" label="collapse" /></Button>
            <Button variant="contained">{editModeSteps}</Button>
            <Button onClick={() => this.changeSteps(true) } onMouseDown={() => this.changeSteps(true) } ><Emoji symbol="➕" label="collapse" /></Button>
          </ButtonGroup>

          <Button
            style={{display: this.state.silosHaveChanges ? undefined : 'none'}}
            onClick={() => this.saveChanges() }
          >Guardar cambios</Button>
        </div>
        {showHistory

        ? (realTemperature
          ? <div>
              <HistogramTC
                temp={realTemperature}
                tempDates={realTemperatureDates}
                silo={siloTem}
              />
              {/* <ul style={{
                display: 'flex',
                paddingLeft: 0,
                listStyle: 'none',
                borderRadius: '.25rem'
              }}>
                {siloTem.rafters.map(r => r.cables.map((c, ci) => 
                  <li key={ci}
                    style={{
                      border: '1px solid black',
                      padding: '7px',
                      width: '36px',
                      textAlign: 'center',
                      background: 'white'
                    }}
                  >
                    <a
                      style={{
                        borderTopLeftRadius: '.2rem',
                        borderBottomLeftRadius: '.2rem'
                      }}
                      href={`#${c.name}`}
                    >
                      {c.name}
                    </a>
                  </li>
                ))}
              </ul>
              <table style={{border: '1px solid black', fontSize: '10px'}}>
                <tbody>
                  <tr>
                    <th>C</th>
                    <th>TC</th>
                    {realTemperatureDates.map((date, i) =>
                      <th key={i}>{date.substring(2, 10)}</th>
                    )}
                  </tr>
                  {siloTem.rafters.map(r =>
                    r.cables.map(c =>
                      c.tcs.map((tc, i) => {
                        let cableInfo = i===0 ? <td rowSpan={c.tcs.length} id={c.name}>{c.name}</td> : null
                        return (
                          <tr key={i} style={{backgroundColor: c.name % 2 === 0 ? '#dedede66' : null }}>
                            {cableInfo}
                            <td>{i+1}</td>
                            {tc.map((t, l) => <td key={l}>{t}</td>)}
                          </tr>  
                        )
                      })
                    )
                  )}
                </tbody>
              </table> */}
            </div>
          : "no history data")
        : <div className="tem-body">
            <div className={"center" + (anySiloSelected ? " ondetails" : "")}>
              <Show
                style={{width: '100%', height: "calc(100vh - 150px)"}}
                if={plantIsSelected}
              >
                <SimpleMap
                  plants={plants}
                  height="calc(100vh - 140px)"
                  onClickPlant={(plantId) => this.changePlant(plantId)}
                />
              </Show>
              <Show
                if={!plantIsSelected}
              >
                <Compass angle={plant ? plant.north : -44} />
                <Blueprint
                  ref={this.blueprintSVG}
                  title={blueprintTitle}
                  plantId={plantId}
                  silos={selectedSilos}
                  summaryTem={this.state.summaryTem}
                  scaleMap={this.state.scaleMap}
                  height="calc(100vh - 140px)"
                  x={this.state.siloMapX}
                  y={this.state.siloMapY}
                  drag={this.drag}
                  startDrag={this.startDrag}
                  endDrag={this.endDrag}
                  avgOrMax={this.state.avgOrMax}
                  limit1={30}
                  limit2={50}
                  // svgRef={this.blueprintSVG}
                  // gRef={this.blueprintG}
                />
              </Show>
            </div>
            <div className={"right" + (anySiloSelected ? " ondetails" : "")}>
              {!this.state.view3D
              ? <TemperatureViewer2D
                  currentDate={currentDate}
                  activeLevel={activeLevel}
                  currentSiloMaxLevel={currentSiloMaxLevel}
                  selectedSilo={selectedSilo}
                  testHeatMap={this.testHeatMap}
                  testTime={this.state.testTime}
                  realSelectedSilo={this.getSelectedSilo()}
                  selectedChanged={this.selectedChanged}
                  emptyAction={() => this.testHeatMap() }
                  maxTemperature={plant ? plant.maxTemperature : 0}
                />
              : <TemperatureViewer3D
                  data={this.state.siloTem}
                  selectedSilo={this.state.selectedSilo}
                  emptyAction={() => this.testHeatMap() }
                  selectedChanged={this.selectedChanged}
                />}
            </div>
          </div>
        }
      </div>
      </DocumentTitle>
    );
  }
}

export default Thermometry;
