import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';
import { title } from '../config';

class Aeration extends Component {
  constructor(props){
    super(props)
    this.state = {
      leftPanelHidden: false,
      magnifyCharts: 0,
      selectedSilo: 1
    }
    this.handleChange = this.handleChange.bind(this)
    this.magnifyCharts = this.magnifyCharts.bind(this)
    this.setSelectedSilo = this.setSelectedSilo.bind(this)
  }
  magnifyCharts(magnify){
    let amount = magnify ? this.state.magnifyCharts+1 : this.state.magnifyCharts-1 
    if(typeof magnify === 'number') amount = magnify
    this.setState({
      magnifyCharts: amount < 0 ? 0 : amount
    })
  }
  handleChange(date, prop){
    let state = prop === "start" ? { startDate: date } : { endDate: date };
    this.setState(state);
  }
  hideLeftPanel(){
    this.setState({
      leftPanelHidden: true
    })
  }
  setSelectedSilo(sid = 0){
    this.setState({
      selectedSilo: sid
    })
  }
  render() {
    return (
      <DocumentTitle title={`${title} - Aereación`}>
        <div className="view aeration">
          Aeration
        </div>
      </DocumentTitle>
    );
  }
}

export default Aeration;
