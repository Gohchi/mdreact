import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';
import { title } from '../config';
import './Support.css';

class Support extends Component {
  render() {
    return (
      <DocumentTitle title={`${title} - Soporte`}>
      <div className="view support">
        Soporte
      </div>
      </DocumentTitle>
    );
  }
}

export default Support;
