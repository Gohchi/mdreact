import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    fontSize: '0.6rem',
    textAlign: 'center',
    '&:last-child': {
      paddingRight: 0
    }
  },
  body: {
    fontSize: 12,
    '&:last-child': {
      paddingRight: '3px'
    }
  },
}))(TableCell);

// const StyledTableRow = withStyles(theme => ({
//   root: {
//     '&:nth-of-type(odd)': {
//       backgroundColor: theme.palette.background.default,
//     },
//   },
// }))(TableRow);

// function createData(name, calories, fat, carbs, protein) {
//   return { name, calories, fat, carbs, protein };
// }

// const rows = [
//   createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
//   createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
//   createData('Eclair', 262, 16.0, 24, 6.0),
//   createData('Cupcake', 305, 3.7, 67, 4.3),
//   createData('Gingerbread', 356, 16.0, 49, 3.9),
// ];

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
  },
  head: {
    '& th': {
      padding: '0px',
      width: '70px'
    }
  },
  table: {
    minWidth: 700,
  },
  tableBody: {
    '& td': {
      padding: '3px',
      border: 0
    }
  },
  tableRow: {
    // '&:hover td': {
    //   fontWeight: 'bold'
    // }
  },
  oddRow: {
    backgroundColor: '#dedede66', //theme.palette.background.default,
    // '&:hover td': {
    //   fontWeight: 'bold'
    // }
  },
  cableName: {
    padding: 0,
    textAlign: 'center',
    fontSize: '28px'
  }
}));


function pickHex(color1, color2, weight) {
  var p = weight;
  var w = p * 2 - 1;
  var w1 = (w/1+1) / 2;
  var w2 = 1 - w1;
  var rgb = [Math.round(color1[0] * w1 + color2[0] * w2),
      Math.round(color1[1] * w1 + color2[1] * w2),
      Math.round(color1[2] * w1 + color2[2] * w2)];

  // console.log(rgb);
  return `${rgb[0]}, ${rgb[1]}, ${rgb[2]}`;
}
function calculateColor(value) {
  const maxValue = 40;
  value = value * 100 / maxValue;

  if(value >= 100){
    return '255, 0, 0';
  }
  const gradient = [
    [ //  0.25: "rgb(0,0,255)", 0.55: "rgb(0,255,0)", 0.85: "yellow", 1.0: "rgb(255,0,0)"
      0, [0,0,255]
    ],
    [
      55, [0,255,0]
    ],
    [
      85, [255,255,0]
    ],
    [
      100, [255,0,0]
    ]
  ]; 

  let colorRange = []
  for (let index = 0; index < gradient.length; index++) {
    let perc = gradient[index][0];
    if(value <= perc) {
      // console.log(value, perc);
      colorRange = [index-1,index];
      break;
    }
  };
  
  //Get the two closest colors
  var firstcolor = gradient[colorRange[0]][1];
  var secondcolor = gradient[colorRange[1]][1];
  
  //Calculate ratio between the two closest colors
  var firstcolor_x = maxValue*(gradient[colorRange[0]][0]/100);
  var secondcolor_x = maxValue*(gradient[colorRange[1]][0]/100)-firstcolor_x;
  var slider_x = maxValue*(value/100)-firstcolor_x;
  var ratio = slider_x/secondcolor_x
  
  //Get the color with pickHex(thx, less.js's mix function!)
  return pickHex(secondcolor, firstcolor, ratio);
}

export default function CustomizedTables({ dates, silo, setCable }) {
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead className={classes.head}>
          <TableRow>
            <StyledTableCell>Cable</StyledTableCell>
            <StyledTableCell>TC</StyledTableCell>
            {dates.map((d, i) => <StyledTableCell key={i}>{d.substring(5, 10)}</StyledTableCell>)}
          </TableRow>
        </TableHead>
        <TableBody className={classes.tableBody}>
          {silo.rafters.map(r =>
            r.cables.map(c =>
              c.tcs.map((tc, i) => {
                let cableInfo = i===0
                  ? <StyledTableCell
                      rowSpan={c.tcs.length}
                      id={c.name}
                      component="th"
                      scope="row"
                      className={classes.cableName}
                      style={{ borderTop: '3px solid', cursor: 'pointer' }}
                      onClick={() => setCable(parseInt(c.name)-1)}
                    >{c.name}</StyledTableCell> : undefined
                return (
                  <TableRow
                    key={i}
                    className={c.name % 2 === 0 ? classes.oddRow : classes.tableRow}
                  >
                    {cableInfo}
                    <StyledTableCell
                      align="center"
                      style={{ borderTop: i===0 ? '3px solid' : undefined }}
                    >{i+1}</StyledTableCell>
                    {tc.map((t, l) =>
                      <StyledTableCell
                        key={l}
                        align="center"
                        style={{
                          backgroundColor: t > 200 ? 'gray' : `rgba(${calculateColor(t > 100 ? t - 100 : t)}, 1)`,
                          borderTop: i===0 ? '3px solid black' : undefined,
                          color: t < 11 ? '#d9d9d9' : undefined,
                          fontWeight: t > 100 ? 'bold' : undefined
                        }}
                      >{t > 100 ? t > 200 ? '--' : t - 100 : t}</StyledTableCell>)}
                  </TableRow>  
                )
              })
            )
          )}
        </TableBody>
      </Table>
    </Paper>
  );
  
  // return (
  //   <Paper className={classes.root}>
  //     <Table className={classes.table}>
  //       <TableHead>
  //         <TableRow>
  //           <StyledTableCell>Dessert (100g serving)</StyledTableCell>
  //           <StyledTableCell align="right">Calories</StyledTableCell>
  //           <StyledTableCell align="right">Fat&nbsp;(g)</StyledTableCell>
  //           <StyledTableCell align="right">Carbs&nbsp;(g)</StyledTableCell>
  //           <StyledTableCell align="right">Protein&nbsp;(g)</StyledTableCell>
  //         </TableRow>
  //       </TableHead>
  //       <TableBody>
  //         {rows.map(row => (
  //           <StyledTableRow key={row.name}>
  //             <StyledTableCell component="th" scope="row">
  //               {row.name}
  //             </StyledTableCell>
  //             <StyledTableCell align="right">{row.calories}</StyledTableCell>
  //             <StyledTableCell align="right">{row.fat}</StyledTableCell>
  //             <StyledTableCell align="right">{row.carbs}</StyledTableCell>
  //             <StyledTableCell align="right">{row.protein}</StyledTableCell>
  //           </StyledTableRow>
  //         ))}
  //       </TableBody>
  //     </Table>
  //   </Paper>
  // );
}
