import React from 'react';
// import { makeStyles } from '@material-ui/core/styles';
import Page from './page';

// const useStyles = makeStyles(() => ({
//   container: {
//     fontSize: '20px',
//     listStyleType: 'none',
//     height: '60px',
//     '& li': {
//       width: '120px',
//       borderRadius: '0 10px 10px 0',
//       backgroundColor: '#6982dd',
//       color: 'white',
//       float: 'left',
//       height: '60px',
//       padding: '2px',
//       textAlign: 'center',
//       // cursor: 'pointer',
//     }
//   },
// }));
// function getDateAsString(){
//   let datetime = new Date()
//   let date = datetime.toLocaleDateString()
//   let time = datetime.toLocaleTimeString()
//   time = time.split(':')
//   return `${date} ${time[0]}:${time[1]}`
// }
function HistogramTC({ temp, tempDates, silo }){
  const [cableIndex, setCable] = React.useState(0);

  // TEST CHARTS
  let chartData = [
    ['Year', 'Sales', 'Expenses', 'Profit'],
    ['2014', 1000, 400, 200],
    ['2015', 1170, 460, 250],
    ['2016', 660, 1120, 300],
    ['2017', 1030, 540, 350],
  ];
  // function ri(min, max) {
  //   return Math.floor(Math.random() * (max - min) ) + min;
  // }
  if(temp){
    let cable = silo.rafters.flatMap(o => o.cables)[cableIndex];
    let base = ['Fechas de mediciones'];
    let tmpData = [base.concat(Array.from(Array(cable.tcs.length).keys()).map(v => `TC: ${++v}`))];
    for(let dateIndex = 0; dateIndex < tempDates.length; dateIndex++){
      if(dateIndex < 35){
        let date = tempDates[dateIndex];
        let temData = [];
        for(let tIndex = 0; tIndex < cable.tcs.length; tIndex++){
          let value = cable.tcs[tIndex][dateIndex];
          value = value > 100 ? value > 200 ? null : value - 100 : value;
          temData.push(value);
        }
        tmpData.push([date.substring(5, 10)].concat(temData));

        // tmpData.push([date.substring(2, 10), ri(1000, 1200), ri(300, 500), ri(150, 700)]);
        // tmpData.push([date.substring(2, 10)], cable.t);
        // siloTem.rafters.map(r =>
        //   r.cables.map(c =>
        //     c.tcs.map((tc, i) => {
        //       // let rafterInfo = i===0 ? <td>r</td>
        //       let cableInfo = i===0 ? <td rowSpan={c.tcs.length} id={c.name}>{c.name}</td> : null
        //       return (
        //         <tr key={i} style={{backgroundColor: c.name % 2 === 0 ? '#dedede66' : null }}>
        //           {cableInfo}
        //           <td>{i+1}</td>
        //           {tc.map((t, l) => <td key={l}>{t}</td>)}
        //           {/* {tc.map((t, l) => <td key={l} style={{backgroundColor: t > 50 ? 'red' : t > 30 ? 'yellow' : '#47ec47' }}>{t}</td>)} */}
        //         </tr>  
        //       )
        //     })
        //   )
        // )
      }
    }
    // console.log(tmpData);
    chartData = tmpData;
  }
  return (
    <Page
      data={chartData}
      dates={tempDates}
      silo={silo}
      setCable={setCable}
      cableIndex={cableIndex}
    />
  );
}

export default HistogramTC;