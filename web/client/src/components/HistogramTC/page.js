import React from 'react'
import Chart from 'react-google-charts'
import Table from './table'

function Page(props){
  const { data, dates, silo, setCable, cableIndex } = props
  return (
    <div>
    {/* <div className={classes.container}> */}
      {/* <li>
        <div>{date}</div>
      </li> */}
      
      {/* <Chart
      // width={'100%'}
      height={'200px'}
      chartType="Timeline"
      loader={<div>Loading Chart</div>}
      data={[
        [
          { type: 'string', id: 'Room' },
          { type: 'string', id: 'Name' },
          { type: 'date', id: 'Start' },
          { type: 'date', id: 'End' },
        ],
        [
          'Magnolia Room',
          'Google Charts',
          new Date(0, 0, 0, 14, 0, 0),
          new Date(0, 0, 0, 15, 0, 0),
        ],
        [
          'Magnolia Room',
          'App Engine',
          new Date(0, 0, 0, 15, 0, 0),
          new Date(0, 0, 0, 16, 0, 0),
        ],
      ]}
      options={{
        timeline: { showRowLabels: false },
        avoidOverlappingGridLines: false,
      }}
      rootProps={{ 'data-testid': '9' }}
    /> */}
      <Chart
        height={'400px'}
        chartType="Line"
        loader={<div>Loading Chart</div>}
        data={data}
        options={{
          // chartArea:{left:20,top:0,width:'50%',height:'75%'},
          chart: {
            title: `Información del cable ${cableIndex + 1}`,
            subtitle: 'seleccione en la lista para filtrar',
          },
          // series: {
          //   // Gives each series an axis name that matches the Y-axis below.
          //   0: { axis: 'Temps' },
          // },
          // axes: {
          //   y: {
          //     Temps: { label: 'Temperatura' },
          //   }
          // }
          
        }}
        rootProps={{ 'data-testid': '2' }}
      />
      {/* <Chart
        // width={'500px'}
        height={'400px'}
        chartType="Bar"
        loader={<div>Loading Chart</div>}
        data={chartData}
        options={{
          // Material design options
          chart: {
            title: 'Company Performance',
            subtitle: 'Sales, Expenses, and Profit: 2014-2017',
          },
        }}
        // For tests
        rootProps={{ 'data-testid': '2' }}
      /> */}
              
      <Table
        dates={dates}
        silo={silo}
        setCable={setCable}
       />
    </div>
  )
}

export default Page