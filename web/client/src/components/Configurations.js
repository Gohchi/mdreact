import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';
import { title } from '../config';
// import { Blueprint } from './Graphs';
import Toolbar from '@material-ui/core/Toolbar'
import Button from '@material-ui/core/Button'
import ButtonGroup from '@material-ui/core/ButtonGroup'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown'
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography';

import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
// import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormHelperText from '@material-ui/core/FormHelperText';

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import AddTodo from '../containers/AddTodo'
import TodoList from '../containers/TodoList'
import { setLoading } from '../actions'
import { connect } from 'react-redux'

// import Chart from 'react-google-charts'

import './Configurations.css';

function TableList({ list, edit, remove }){
  const useStyles = makeStyles(theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing(3),
      overflowX: 'auto',
    },
    table: {
      minWidth: 650,
    },
  }));

  const classes = useStyles();  
  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Nombre</TableCell>
            <TableCell>#</TableCell>
            <TableCell>Tipo</TableCell>
            <TableCell>Tiras/Aros</TableCell>
            <TableCell>Vac&iacute;o</TableCell>
            <TableCell>Acciones</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {list.map((o, i) => (
            <TableRow key={i}>
              <TableCell>{o.name}</TableCell>
              <TableCell>{o.siloNumber}</TableCell>
              <TableCell>{o.type === 'cell' ? 'Celda' : 'Silo'}</TableCell>
              <TableCell>{o.rafters.length}</TableCell>
              <TableCell>{o.empty ? 'Sí' : 'No'}</TableCell>
              <TableCell>
                <ButtonGroup size="small" aria-label="small outlined button group">
                  <Button color="primary" onClick={() => edit(o)}>Editar</Button>
                  <Button color="secondary" onClick={() => remove(o.id)}>Quitar</Button>
                </ButtonGroup>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  )
}
class NewSilo extends Component {
  constructor(props){
    super(props)
    const baseSize = 140;
    this.state = {
      errorMessage: "",
      rafterLengthErr: "",
      silo: {
        name: "",
        type: "silo",
        x: baseSize,
        y: baseSize,
        width: baseSize * 2,
        height: baseSize * 2,
        siloNumber: 1,
        rafters: [{
          radius: baseSize,
          cables: Array(0)
        },{
          radius: 110,
          cables: Array(0)
        },{
          radius: 70,
          cables: Array(0)
        }],
        empty: false
      },
      selectedRafterId: 1,
      forceUpdate: false,

      // temp
      newSiloType: 'cell'
    }
    this.newPlant = React.createRef()
    this.rafterLengthRef = React.createRef()
    this.handleSubmit = this.handleSubmit.bind(this)
    this.createSilo = this.createSilo.bind(this)

    this.testSiloCreation = this.testSiloCreation.bind(this)
    this.changeRafterLength = this.changeRafterLength.bind(this)
    this.changeSelectedRafter = this.changeSelectedRafter.bind(this)
    this.changeCableLength = this.changeCableLength.bind(this)
    this.changeSelectedRafterRadius = this.changeSelectedRafterRadius.bind(this)
    this.removeRafter = this.removeRafter.bind(this)
    this.changePreview = this.changePreview.bind(this)
  }
  
  handleSubmit(event){ 
    event.preventDefault();
    let form = this.newPlant.current;
    let id = form.elements["id"].value;
    // console.log('Values:', this.props.plantId, id, name, type)
    // if(name === ""){
    //   this.setState({
    //     errorMessage: 'Debe indicar un nombre'
    //   })
    // } else {
    this.createSilo(
      Object.assign({}, this.state.silo, {
        "plant": this.props.plantId,
        "id": id === 'new' ? undefined : id,
        "name": form.elements["name"].value,
        "siloNumber": form.elements["siloNumber"].value,
        "type": form.elements["type"].value
      }))
      .then(() => this.props.closeEdit(this.props.plantId))
      .catch(err => console.error(err));
    // }
  }
  
  createSilo = async (data) => {
    const response = await fetch('/silo', {
      method: 'post',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    })
    const body = await response.json()

    if (response.status !== 200) {
      console.error("something went wrong...");
    }
    return body;
  }
  changeRafterLength(event) {
    const value = (event.target.validity.valid) ? event.target.value : 1;
    // console.log(value)
    let silo = Object.assign({}, this.state.silo);
    if (silo.rafters.length < value){
      const prevRafter = silo.rafters[silo.rafters.length -1];
      const radius = prevRafter.radius - (15 * (1 + silo.rafters.length / 10 ));
      if (radius < 0) {
        this.setState({
          rafterLengthErr: "No pueden agregarse más aros"
        })
        return;
      } 
      silo.rafters.push({
        radius: radius,
        cables: Array(0)
      })
    } else if (silo.rafters.length > value) {
      silo.rafters.pop();
      //let popped = 
      // if(popped.cables.length) {
      //   this.setState({
      //     errorMessage: "El aro contiene "
      //   })
      //   return;
      // }
    } else {
      return;
    }
    
    this.setState({
      silo,
      selectedRafterId: 1,
      forceUpdate: true
    })
  }
  changeCableLength(event) {
    const value = (event.target.validity.valid) ? event.target.value : 1;
    let silo = Object.assign({}, this.state.silo);
    let selectedRafter = silo.rafters[parseInt(this.state.selectedRafterId) - 1];
    if (selectedRafter.cables.length < value){
      selectedRafter.cables.push({});
    } else if (selectedRafter.cables.length > value) {
      selectedRafter.cables.pop();
    } else {
      return;
    }
    selectedRafter.cables.forEach((cable, i) => {
      cable.angle = 360 / selectedRafter.cables.length * i;
    });
    let cableName = 1;
    silo.rafters.forEach(rafter => {
      rafter.cables.forEach(cable => cable.name = cableName++);
    });
    this.setState({
      silo,
      // selectedRafter: 1,
      forceUpdate: true
    })
  }
  changeSelectedRafter(event) {
    const value = (event.target.validity.valid) ? event.target.value : 1;
    let { silo } = this.state;
    if (value > 0 && silo.rafters.length >= value) {
      this.setState({
        selectedRafterId: value,
        forceUpdate: true
      })
    }
  }
  removeRafter(){
    const selectedRafterId = parseFloat(this.state.selectedRafterId) - 1;
    if(selectedRafterId){
      let silo = Object.assign({}, this.state.silo);
      delete silo.rafters[selectedRafterId];
      this.setState({
        silo,
        selectedRafterId: 1,
        forceUpdate: true
      })
    }
  }
  getValueFromEvent(event){
    return parseFloat((event.target.validity.valid) ? event.target.value : 1);
  }
  changeSelectedRafterRadius(event){
    const value = this.getValueFromEvent(event);
    // console.log(value)
    let silo = Object.assign({}, this.state.silo);
    const selectedRafterId = parseFloat(this.state.selectedRafterId);
    const selectedRafter = silo.rafters[selectedRafterId - 1];
    if (selectedRafterId === 1){
      if(silo.rafters.length > 1){
        const nextRafter = silo.rafters[1];
        if(value <= parseFloat(nextRafter.radius)) return; 
      }
      selectedRafter.radius = value;
      silo.x = value;
      silo.y = value;
      this.setState({
        silo,
        forceUpdate: true
      })
      return;
    } else {
      const prevRafter = silo.rafters[selectedRafterId - 2];
      if(value >= parseFloat(prevRafter.radius)) return; 
      if(silo.rafters.length > selectedRafterId){
        const nextRafter = silo.rafters[selectedRafterId];
        if(value <= parseFloat(nextRafter.radius)) return; 
      }
    }
    selectedRafter.radius = value;
    this.setState({
      silo,
      forceUpdate: true
    })
  }
  testSiloCreation(){
    let form = this.newPlant.current;
    let id = form.elements["id"].value;
    let params = Object.assign({}, this.state.silo, {
      "plant": this.props.plantId,
      "id": id === 'new' ? undefined : id,
      "name": form.elements["name"].value,
      "siloNumber": form.elements["siloNumber"].value,
      "type": form.elements["type"].value
    });
    console.log(params);
    // return;
    // let { rafters, height, width } = this.state

    // let updatedRafters = []
    // let max = (() => {
    //   var total = 0
    //   for (let i = 0, _len = rafters.length; i < _len; i++ ) {
    //       if(rafters[i].cables)
    //         total += rafters[i].cables.length
    //   }
    //   return total
    // })()
    // rafters.forEach((rafter, index) => {
    //   if (index === 0){
    //     height = ((rafter.radius * 2) * 1.1)
    //     width = height
    //   }
    //   updatedRafters.push({
    //     radius: rafter.radius,
    //     cables: (!rafter.cables || rafter.cables.length === 0)
    //       ? []
    //       : Array.from({ length: rafter.cables.length }, (e, i) => {
    //         // console.log(360 / rafter.cables.length, 'rafter', rafter.radius)
    //         return {
    //           angle: 360 / rafter.cables.length * i,
    //           name: (max - rafter.cables.length) + i + 1
    //         }
    //       })
    //   })
    //   if(rafter.cables)
    //     max -= rafter.cables.length
    // })

    // this.setState({
    //   rafters: updatedRafters,
    //   height,
    //   width
    // })
  }
  changePreview() {
    // console.log(event.target.value);
    this.setState({ forceUpdate: true });
  }
  shouldComponentUpdate(nextProps, nextStates){
    let hasDiff = false;
    const selectedSilo = nextProps.selectedSilo;
    const currentSelectedSilo = this.props.selectedSilo;
    for (let prop = 0; prop < selectedSilo.length; prop++){
      if(selectedSilo[prop] !== currentSelectedSilo[prop])
        hasDiff = true;
    }
    // if(this.state.silo.rafters.length != nextStates.silo.rafters.length){
    //   hasDiff = true;
    // }
    if (hasDiff) {
      this.newPlant.current.elements["id"].value = selectedSilo.id
      this.newPlant.current.elements["name"].value = selectedSilo.name || ""
      this.newPlant.current.elements["siloNumber"].value = selectedSilo.siloNumber || ""
      this.newPlant.current.elements["type"].value = selectedSilo.type || "silo"
      if (selectedSilo.id !== 'new'){
        const radius = selectedSilo.rafters[0].radius;
        nextStates.silo = Object.assign({}, selectedSilo, { x: radius, y: radius});
      }
      return true;
    }
    if (this.state.errorMessage !== nextStates.errorMessage)
    {
      console.log(nextStates.errorMessage);
      return true;
    }
    if (this.state.rafterLengthErr !== nextStates.rafterLengthErr)
    {
      if(nextStates.rafterLengthErr){
        if(this.timer){
          clearTimeout(this.timer)
        }

        this.timer = setTimeout(() => {
          this.setState({
            rafterLengthErr: "",
          });
        }, 3000);
      }
      return true;
    }
    if (nextStates.forceUpdate){
      nextStates.forceUpdate = false;
      return true;
    }
    return false;
  }
  render(){
    // const { silo, selectedRafterId } = this.state;
    // const selectedRafter = silo.rafters[parseInt(selectedRafterId) - 1];
    
    // let form = this.newPlant.current;
    // let type = "";
    // if (form) {
    //   type = form.elements["type"].value;
    // }

    return (
      <form
        ref={this.newPlant} 
        onSubmit={this.handleSubmit}>

        <ButtonGroup>
          <Button color="secondary" type="submit">{this.props.selectedSilo.id === 'new' ? 'Agregar' : 'Actualizar'}</Button>
          <Button color="secondary" onClick={() => this.props.closeEdit()}>Cancelar</Button>
          <Button color="secondary" onClick={() => this.testSiloCreation()}>Test</Button>
        </ButtonGroup>

        <FormHelperText error>{this.state.errorMessage}</FormHelperText>
        <input type="hidden" name="id" />
        <TextField
          name="name"
          label="Ingrese el nombre"
          // error={anyError}
          variant="outlined"
          margin="normal"
          required
          fullWidth
          autoFocus
        />
        <TextField
          name="siloNumber"
          label="Ingrese el n&uacute;mero"
          // error={anyError}
          variant="outlined"
          margin="normal"
          required
          fullWidth
        />
        <FormControl component="fieldset">
          <FormLabel component="legend">Tipo</FormLabel>
          <RadioGroup aria-label="Tipo"
            name="type"
            value={this.state.newSiloType}
            onChange={event => this.setState({ newSiloType: event.target.value, forceUpdate: true }) }
          >
            <FormControlLabel value="silo" control={<Radio />} label="Silo" />
            <FormControlLabel value="cell" control={<Radio />} label="Celda" />
          </RadioGroup>
        </FormControl>

        {/* <Form.Control.Feedback type="invalid">{this.state.errorMessage}</Form.Control.Feedback>
        <Form.Control type="hidden" name="id" />
        <Form.Control type="text" name="name" required placeholder="Ingrese el nombre" isInvalid={this.state.errorMessage} />
        <Form.Control type="number" name="siloNumber" min="0" placeholder="Ingrese el n&uacute;mero" isInvalid={this.state.errorMessage} />
        <Form.Group onChange={ref => this.changePreview(ref)}>
          <Form.Check type="radio" name="type" value="silo" label="Silo" />
          <Form.Check type="radio" name="type" value="cell" label="Cell" />
        </Form.Group>
        <Form.Row style={{display: type === "silo" ? undefined : "none"}}>
          <Col md={3}>
            <Form.Label>Cantidad de aros</Form.Label>
            
            <Overlay
              show={!!this.state.rafterLengthErr}
              target={this.rafterLengthRef.current}
              placement="bottom"
              container={this}
              containerPadding={20}
            >
              <Popover title="Error">
                No se pueden agregar aros m&aacute;s peque&ntilde;os
              </Popover>
            </Overlay>
            <Form.Control
              type="number"
              min="1"
              ref={this.rafterLengthRef}
              value={silo.rafters.length}
              onChange={ref => this.changeRafterLength(ref)}/>
            <Form.Label>Aro seleccionado</Form.Label>
            <InputGroup>
              <Form.Control type="number"
                value={selectedRafterId}
                onChange={ref => this.changeSelectedRafter(ref)}/>
              <InputGroup.Append>
                <Button color="secondary" disabled={parseInt(selectedRafterId)===1} onClick={() => this.removeRafter()}>Quitar</Button>
              </InputGroup.Append>
            </InputGroup>
            <Form.Label>Cantidad de cables</Form.Label>
            <Form.Control type="number" min="0" disabled={parseInt(selectedRafterId)===1}
              value={selectedRafter.cables.length}
              onChange={ref => this.changeCableLength(ref)}/>
            <Form.Label>Radio de aro</Form.Label>
            <Form.Control type="number"
              value={selectedRafter.radius}
              onChange={ref => this.changeSelectedRafterRadius(ref)}/>
          </Col>
          <Col>
            <Form.Label>Vista previa</Form.Label>
            <Blueprint silo={silo} magnifyCharts={0} width={200} height={200} />
          </Col>
        </Form.Row> */}
      </form>
    )
  }
}

class Configurations extends Component {
  constructor(props){
    super(props)
    this.state = {
      plants: [],
      silos: [],
      selectedSilo: {},
      // selectedSilo: { id: 'new' }, // test

      files: null,
      showAddPlant: false,
      errorMessage: "",
      menuOpen: false
    }
    this.menuRef = React.createRef()

    this.handleFileSelect = this.handleFileSelect.bind(this)
    this.handleTemFile = this.handleTemFile.bind(this)
    this.changePlant = this.changePlant.bind(this)
    this.closeEdit = this.closeEdit.bind(this)
    this.editSilo = this.editSilo.bind(this)
    this.removeSilo = this.removeSilo.bind(this)
    
    this.submitAddPlant = this.submitAddPlant.bind(this)
    this.getSelectedPlant = this.getSelectedPlant.bind(this)
  }
  submitAddPlant(event) {
    event.preventDefault();
    const { setLoading } = this.props;
    const data = new FormData(event.target);
    setLoading(true);
    console.log('asdasd new plant' + data.get('name'));
    this.createPlant(data.get('name'))
      .then(res => {
        setLoading(false);
        this.updatePlants(res.newPlantId)
      })
      .catch(err => {
        setLoading(false);
        console.error(err)
      });
  }
  getSelectedPlant(){
    const activePlants = this.state.plants.filter(plant => plant.active);
    if (activePlants) {
      return activePlants[0];
    }
    return {};
  }
  removeSilo(siloId) {
    // if (window.confirm('¿Está seguro de quitar este registro?')){
      this.createSiloCall(siloId)
        .then(() => this.updatePlants(this.getSelectedPlant().id))
        .catch(err => console.error(err));
    // }
  }
  createSiloCall = async (id) => {
    const response = await fetch('/silo', {
      method: 'delete',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "id": id
      })
    })
    const body = await response.json()

    if (response.status !== 200) {
      console.error("something went wrong...");
    }
    return body;
  }
  editSilo(silo){
    this.setState({
      selectedSilo: silo
    }) 
  }
  closeEdit(plantIdToUpdate){
    if (plantIdToUpdate) {
      this.updatePlants(plantIdToUpdate);
    } else {
      this.setState({
        selectedSilo: {},
        errorMessage: null
      }) 
    }
  }
  changePlant(plantId){
    this.setState({
      // selectedPlant: plant,
      plants: this.state.plants.map(o => Object.assign({ ...o }, { active: o.id === plantId })),
      selectedSilo: {}
    })
  }
  handleTemFile(event){
    const { setLoading } = this.props;
    let files = event.target.files;
    if(files.length === 2){
      let hasTEM = false;
      let hasDIA = false;
      for(let i = 0; i < 2; i++){
        if (!hasTEM) hasTEM = /tem$/i.test(files[i].name)
        if (!hasDIA) hasDIA = /dia$/i.test(files[i].name)
      }
      if(hasTEM && hasDIA){
        setLoading(true);
        this.doUpload(files, '/upload/tem')
          .then(() => {
              setLoading(false);
              this.updatePlants(this.getSelectedPlant().id);
            }
          )
      }
    }
  }
  handleFileSelect (event) {
    const { setLoading } = this.props;
    setLoading(true);
    this.doUpload(event.target.files, '/upload/fcb')
      .then(res => {
          setLoading(false);
          this.updatePlants(this.getSelectedPlant().id);
        }
      )
  }
  doUpload = async (files, uri) => {
    const form = new FormData();
    
    // console.log(this.state.selectedPlant.id);
    for (var i in files) {
      var file = files[i];
      form.append('file', file);      
    }
    form.append('plantId', this.getSelectedPlant().id);
    const response = await fetch(uri, {
      method: 'post',
      headers: {
          'Accept': 'text/html',
          // 'Content-Type': 'multipart/form-data' //'application/json',
      },
      body: form
    })
    const body = await response.json();
    return body;
  }
  createPlant = async (name) => {
    const response = await fetch('/plants', {
      method: 'post',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "name": name,
        "getAll": true
      })
    })
    const body = await response.json()

    if (response.status !== 200) {
      console.error("something went wrong...");
    }
    return body;
  }
  asParams(params){
    const esc = encodeURIComponent;
    const query = Object.keys(params)
        .map(k => esc(k) + '=' + esc(params[k]))
        .join('&');
    return "?" + query;
  }
  getPlants = async () => {
    const params = this.asParams({
      "config": true
    });
    const response = await fetch('/plants' + params, {
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
      }
    })
    if (response.status === 401) {
      // this.props.openLogin();
    }
    const body = await response.json()
    if (response.status !== 200) {
      console.error("something went wrong...");
    }
    return body;
  }
  UNSAFE_componentWillReceiveProps(props) {
    const { refresh } = this.props
    if (props.refresh !== refresh) {
      this.updatePlants();
    }
  }
  updatePlants(plantIdDefault){
    this.getPlants()
      .then(res => {
        if(res.plants)
        {
          let states = {
            plants: res.plants,
            silos: res.silos,
            showAddPlant: false,
            selectedPlant: {}
          };
          
          if (plantIdDefault) {
            states.plants = states.plants.map(plant => {
              plant.active = (plant.id === plantIdDefault);
              return plant;
            })
            states.selectedSilo = {};
            states.errorMessage = null;
          }

          this.setState(states)
        }
      })
  }
  UNSAFE_componentWillMount(){
    this.updatePlants();
  }
  render() {
    const { plants, silos, /*selectedPlant,*/ selectedSilo } = this.state;
    function showIfExists(obj){
      return {
        style: {display: !obj.id ? "none" : ""}
      }
    }
    
    let selectedPlant = {}
    const selectedPlants = plants.filter(plant => plant.active);
    const hasActivePlant = !!selectedPlants.length; 
    if (selectedPlants.length){
      selectedPlant = selectedPlants[0];
    }
    
    let siloList = silos.filter(o => selectedPlant !== null && o.plant === selectedPlant.id);

    const { menuOpen } = this.state;
    
    return (
      <DocumentTitle title={`${title} - Configuraciones`}>
      <div className="view configurations" style={{backgroundColor: "#8cbaff"}} >
        {/* <AddTodo /> */}
        {/* <TodoList /> */}
{/*         
        <Chart
              // width={'100%'}
              height={'200px'}
              chartType="Timeline"
              loader={<div>Loading Chart</div>}
              data={[
                [
                  { type: 'string', id: 'Room' },
                  { type: 'string', id: 'Name' },
                  // { type: 'date', id: 'Start' },
                  // { type: 'date', id: 'End' },
                  { type: 'number', id: 'Start' },
                  { type: 'number', id: 'End' },
                ],
                [
                  'Magnolia Room',
                  'Google Charts',
                  1451566800000, 1451653200000
                  // new Date(0, 0, 0, 14, 0, 0),
                  // new Date(0, 0, 0, 15, 0, 0),
                ],
                [
                  'Magnolia Room',
                  'App Engine',
                  1451739600000, 1451912400000
                  // new Date(0, 0, 0, 15, 0, 0),
                  // new Date(0, 0, 0, 16, 0, 0),
                ],
              ]}
              options={{
                timeline: { showRowLabels: false },
                avoidOverlappingGridLines: false,
              }}
              rootProps={{ 'data-testid': '9' }}
            />
        <Chart
          // width={'500px'}
          height={'400px'}
          chartType="Bar"
          loader={<div>Loading Chart</div>}
          data={[
            ['Year', 'Sales', 'Expenses', 'Profit'],
            ['2014', 1000, 400, 200],
            ['2015', 1170, 460, 250],
            ['2016', 660, 1120, 300],
            ['2017', 1030, 540, 350],
          ]}
          options={{
            // Material design options
            chart: {
              title: 'Company Performance',
              subtitle: 'Sales, Expenses, and Profit: 2014-2017',
            },
          }}
          // For tests
          rootProps={{ 'data-testid': '2' }}
        /> */}
        <Toolbar style={{backgroundColor: "#6982dd"}}>
          <ButtonGroup 
            variant="contained"
            color="primary"
          >
            <Button
              variant="contained"
              size="small"
              ref={(ref) => { this.menuRef = ref}}
              aria-owns={menuOpen ? 'menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={() => this.setState({ menuOpen: !menuOpen })}
            >
              Plantas
              <ArrowDropDownIcon />
            </Button>
            {/* <Button variant="primary">Plantas</Button> */}
            <Button onClick={() => this.setState({showAddPlant: true}) }>Nueva</Button>
          </ButtonGroup>

        </Toolbar>
        <Popper open={menuOpen} anchorEl={this.menuRef} transition disablePortal style={{ zIndex: 1 }}>
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{
                transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
              }}
            >
              <Paper id="menu-list-grow">
                <ClickAwayListener onClickAway={() => this.setState({ menuOpen: false} )}>
                  <MenuList>
                    {plants.map((plant, i) =>
                      <MenuItem
                        key={i} 
                        onClick={() => {this.changePlant(plant.id); this.setState({ menuOpen: false} )}}
                        selected={plant.active}
                      >
                        {plant.name}
                      </MenuItem>
                    )}
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
        <Box style={{padding: "20px"}} m={1}>
          <div style={{display: (!hasActivePlant ? "none" : "")}}>
            <Typography variant="h3" noWrap>
              {selectedPlant ? selectedPlant.name : ""}
            </Typography>
            <Typography variant="h6" noWrap>
              Silos / Celdas
            </Typography>
          </div>
          <div style={{display: (!hasActivePlant || selectedSilo.id ? "none" : "")}}>
            <Button
              variant="contained"
              onClick={() => this.setState({ selectedSilo: { id: 'new' } })}
            >Nuevo</Button>
            <div className="MuiButtonBase-root MuiButton-root MuiButton-contained">
              <label className="MuiButton-label" htmlFor="upload-plant-files">Importar con FCB</label>
              <input
                type="file"
                name="files[]"
                id="upload-plant-files"
                accept=".fcb"
                onInput={this.handleFileSelect}
                style={{display: 'none'}} />
            </div>
            <div className="MuiButtonBase-root MuiButton-root MuiButton-contained">
              <label className="MuiButton-label" htmlFor="upload-plant-temperature">Importar temperaturas</label>
              <input
                type="file"
                name="files[]"
                id="upload-plant-temperature" 
                accept=".dia,.tem" 
                multiple
                onInput={this.handleTemFile}
                style={{display: 'none'}} />
            </div>
            {siloList.length
            ? <TableList
                list={siloList}
                edit={(o) => this.setState({ selectedSilo: o })}
                remove={(id) => this.removeSilo(id)}
              />
            : <Typography variant="h4" noWrap>
              Sin silos o celdas
            </Typography>}
          </div>

          <div  {...showIfExists(selectedSilo)}>
            <NewSilo
              plantId={selectedPlant.id}
              selectedSilo={selectedSilo}
              closeEdit={this.closeEdit} />
          </div>              
          
            
          <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={this.state.showAddPlant}
            onClose={()=> this.setState({showAddPlant: false})}
          >
            <div style={{
                position: 'absolute',
                width: 400,
                backgroundColor: 'white',
                border: '2px solid #000',
                // boxShadow: theme.shadows[5],
                padding: 15,//theme.spacing(2, 4, 3),
                top: '10%',
                left: '35%'
              }}>
              <h2 id="simple-modal-title">Agregar planta</h2>
              <p id="simple-modal-description">
                Antes de continuar hay que asignarle un nombre a la planta
              </p>

              <form onSubmit={this.submitAddPlant}>
                      
                <TextField
                  // error={anyError}
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="plant-name"
                  label="Ingrese el nombre"
                  name="name"
                  autoComplete="plant-name"
                  autoFocus
                />
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  // className={classes.submit}
                >
                  {"Agregar"}
                </Button>
            </form>
            </div>
          </Modal>
        </Box>
      </div>
      
      </DocumentTitle>
    );
  }
}

export default connect(undefined, { setLoading })(Configurations);
