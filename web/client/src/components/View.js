import React, { Component } from 'react';
import Histogram from './Histogram';
import Aeration from './Aeration';
import Configurations from './Configurations';
import Thermometry from './Thermometry';
import Support from './Support';
import Login from './Login';
import { connect } from 'react-redux'
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import './View.css';

function RouteOrRedirect({render: Render, authed, location, path}){
  if(authed)
    return <Route path={path} render={Render} />
  return <Redirect to={{pathname: '/login', state: {from: location}}} />
}

const mapStateToProps = (state) => {
  return {
    loggedIn: state.userData.loggedIn
  }
}
class View extends Component {
  render() {
    const { loggedIn } = this.props;
    const authed = !!loggedIn;
    
    return (
      <Switch>
        <Route
          path="/login"
          render={(router) => 
          <Login
            authed={authed}
            // handleLogin={handleLogin}
            // refresh={this.props.refresh}
            // openLogin={this.props.openLogin}
            location={router.location}  
          />} />
        <RouteOrRedirect
          authed={authed}
          path="/thermometry"
          render={() => <Thermometry refresh={this.props.refresh} />}
        />
        <RouteOrRedirect
          authed={authed}
          path="/aeration"
          render={() => <Aeration />}
        />
        <RouteOrRedirect
          authed={authed}
          path="/histogram"
          render={() => <Histogram />}
        />
        <RouteOrRedirect
          authed={authed}
          path="/fillingUp"
          render={() => <div>Llenado</div>}
        />
        <RouteOrRedirect
          authed={authed}
          path="/weather"
          render={() => <div>Clima</div>}
        />
        <RouteOrRedirect
          authed={authed}
          path="/configurations"
          render={(router) => <Configurations refresh={this.props.refresh} {...router} />} />
        <RouteOrRedirect
          authed={authed}
          path="/announcements"
          render={() => <div>Avisos</div>}
        />
        <RouteOrRedirect
          authed={authed}
          path="/support"
          render={() => <Support refresh={this.props.refresh}></Support>}
        />
        <RouteOrRedirect
          authed={authed}
          render={() => <div>Avisos</div>}
          // render={(router) => <Configurations refresh={this.props.refresh} {...router} />}
        />
      </Switch>
    )
  }
}

export default withRouter(connect(mapStateToProps)(View));
