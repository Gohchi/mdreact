import React from 'react';
import { Redirect } from 'react-router-dom';
import { setLogIn, setLoading, setLoginError, enqueueSnackbar } from '../../actions'
import { connect } from 'react-redux'
import Page from './page';
// import { awsConfig } from '../../config'

// import { CognitoUserPool, CognitoUser, AuthenticationDetails } from 'amazon-cognito-identity-js';

// const poolData = {
//   UserPoolId: awsConfig.cognito.userPoolId,
//   ClientId: awsConfig.cognito.userPoolClientId
// };

// var userPool = new CognitoUserPool(poolData);

async function doLogin(data) {
  try {
      
    const response = await fetch('/login', {
      method: 'post',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
      },
      // mode: "cors",
      body: JSON.stringify({
        "email": data.email,
        "password": data.password
      })
    })
    const body = await response.json()
    return body;
  }
  catch(e){
    console.error(e)
  }
}
// function expressLogin(email, password, setLogIn, setLoading, setLoginError) {
//   doLogin({ email: email, password: password })
//     .then(res => {
//       setLoading(false);
//       if(res.isNotLoggedIn){
//         setLoginError(true, res.message)
//       } else {
//         setLoginError(false)
//         setLogIn(true)
//       }
//       // if(res.isNotLoggedIn){
//       //   this.setState({
//       //     showLogin: true,
//       //     hideLoading: true
//       //   })
//       // }else{
//       //   this.props.setUserData(res.user)
//       //   this.setState({
//       //     userData: res.user,
//       //     hideLoading: true,
//       //     refreshCurrentView: !this.state.refreshCurrentView
//       //   })
//       //   this.callBackendAPI()
//       //     .then(res => this.setState({ data: res.express }))
//       //     .catch(err => console.error(err));
//       // }
//     })
//     .catch(err => console.error(err));
// }
//loginError

const mapStateToProps = (state) => {
  return {
    error: state.misc.loginError,
  }
}

export default connect(mapStateToProps, { setLogIn, setLoading, setLoginError, enqueueSnackbar })(({authed, location, router, setLogIn, setLoading, setLoginError, error, enqueueSnackbar}) => {
  if(authed){
    return <Redirect to={{pathname: '/', state: {from: location}}} />
  }
  
  const snackNotification = (message, variant) => {

    // NOTE:
    // if you want to be able to dispatch a `closeSnackbar` action later on, 
    // you SHOULD pass your own `key` in the options. `key` can be any sequence
    // of number or characters, but it has to be unique to a given snackbar. 
    enqueueSnackbar({
        message,
        options: {
            key: new Date().getTime() + Math.random(),
            variant,
            // action: key => (
            //     <Button onClick={() => closeSnackbar(key)}>dissmiss me</Button>
            // ),
        },
    });
  };
    
  const handleLogin = (event) => { 
    event.preventDefault();
    const data = new FormData(event.target);
    setLoading(true);
    // console.log(AuthenticationDetails);
      
    const email = data.get('email');
    const password = data.get('password');

    
    doLogin({ email: email, password: password })
      .then(res => {
        setLoading(false);
        if(res.isNotLoggedIn){
          setLoginError(true, res.message)
        } else {
          snackNotification('Logged in!', 'success')
          setLoginError(false)
          setLogIn(true)
        }
      })
      .catch(err => console.error(err));
    // var authenticationDetails = new AuthenticationDetails({
    //     Username: email,
    //     Password: password
    // });

    // var cognitoUser = createCognitoUser(email);
    // cognitoUser.authenticateUser(authenticationDetails, {
    //     onSuccess: session => {
    //       snackNotification('Validado correctamente con AWS Cognito', 'success')
    //       // expressLogin(email, password, setLogIn, setLoading, setLoginError)
    //       setLoginError(false)
    //       setLogIn(true, session.getIdToken().getJwtToken())
    //       setLoading(false);
    //     },
    //     onFailure: res => {
    //       setLoginError(true, res.message)
    //       setLoading(false);
    //     }
    // });
    
    // function createCognitoUser(email) {
    //     return new CognitoUser({
    //         Username: email,
    //         Pool: userPool
    //     });
    // }
  }

  return (
    <Page
      handleLogin={handleLogin}
      error={error}
    />
  );
});