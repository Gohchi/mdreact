import React, {Component} from 'react'

import Chip from '@material-ui/core/Chip';
import Slider from './Slider'
import CoverButton from './CoverButton'
import { SiloMap } from './Graphs'
import TempMeter from './TempMeter'
// import { ReactComponent as TempMeter } from '../image/svg/temperature-meter.svg';
import './TemperatureViewer2D.css'

class TemperatureViewer2D extends Component {
  constructor(props){
    super(props)
    this.state = {
      // level: 2,
      // selectedIndex: 0,
      // perspectiveY: 0,
      // perspectiveZ: 0,
      // perspective: 1000
    }
    // this.handleChange = this.handleChange.bind(this)
  }
  dateDiffInDays(dt1, dt2) {
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
  }
  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    const { selectedSilo, realSelectedSilo } = this.props
    if(selectedSilo && realSelectedSilo && selectedSilo.siloNumber !== realSelectedSilo.siloNumber) {
      this.props.selectedChanged()
    }
  }
  render(){
    const { currentDate, activeLevel, /*currentSiloMaxLevel,*/ selectedSilo, testHeatMap, emptyAction, maxTemperature } = this.props;
    if(!selectedSilo) {
      setTimeout(() => emptyAction(), 100);
      return (
        // <CoverButton
        //   message="Presione para obtener la información detallada"
        //   onClick={emptyAction}
        // />
        <CoverButton
          message="Cargando información detallada..."
        />
      );
    }
    let daysFromNow;
    if(currentDate){
      daysFromNow = new Date(currentDate);

    }
    // let levels = [];
    // for (let i = currentSiloMaxLevel; i > 0; i--){
    //   levels.push(
    //     <div
    //       key={i}
    //       className={`level-selector-item${(activeLevel===i?' active' : '')}`}
    //       onClick={() => {
    //         testHeatMap(i);
    //       }}
    //     >
    //       <span>{i}</span>
    //     </div>
    //   )
    // }
    // levels = <div className="level-selector">{levels}</div>
    return (
      <div className="temperatureviewer2d demo-wrapper">
        {/* <Badge className="level-info" variant="secondary">Nivel {activeLevel}</Badge> */}
        {/* <Slider
          orient="vertical"
          max={currentSiloMaxLevel}
          value={activeLevel}
          handler={value => {
            // this.setState({ activeLevel: value });
            testHeatMap(value);
          }}
        /> */}
        <div className="heatmap-container">
          <TempMeter max={maxTemperature} height={300} />
          <div className="heatmap" style={{position: "relative"}}>
            <canvas className="heatmap-canvas" style={{position: 'absolute', left: "0px", top: "0px", width: 0}}></canvas>
          </div>
        </div>
        <div className="date-selector">
          
          <Chip
            label={`${currentDate.substring(8, 10)}-${currentDate.substring(5, 7)}-${currentDate.substring(0, 4)} ${currentDate.substring(11, 16)}`}
            style={{ margin: 1 }}
          />
          
          <Chip
            label={`Hace ${daysFromNow ? this.dateDiffInDays(daysFromNow, new Date()) : "-"} días`}
            style={{ margin: 1 }}
          />
          
          <Slider
            max={35}
            value={this.props.testTime}
            handler={value => {
              testHeatMap(null, value);
            }}
          />
        </div>

        <SiloMap width={250} height={200}
          silo={selectedSilo}
          selected={activeLevel}
          changeLevel={(i) => {
            testHeatMap(i);
          }}
        />
        {/* {levels} */}
      </div>
    )
  }
}

export default TemperatureViewer2D