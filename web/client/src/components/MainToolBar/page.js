import React from 'react';
// import { Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
// import InputBase from '@material-ui/core/InputBase';
import Badge from '@material-ui/core/Badge';
// import { withStyles } from '@material-ui/core/styles'
// import Tooltip from '@material-ui/core/Tooltip';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MenuIcon from '@material-ui/icons/Menu';
// import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MailIcon from '@material-ui/icons/Mail';
// import BrightnessHighIcon from '@material-ui/icons/BrightnessHigh'
// import ToysIcon from '@material-ui/icons/Toys'
// import EqualizerIcon from '@material-ui/icons/Equalizer';
// import CloudIcon from '@material-ui/icons/Cloud'
// import FilterVintageIcon from '@material-ui/icons/FilterVintage' 
// import SettingsIcon from '@material-ui/icons/Settings';
import NotificationsIcon from '@material-ui/icons/Notifications';
// import BuildIcon from '@material-ui/icons/Build';
import MoreIcon from '@material-ui/icons/MoreVert';
import ReactIcon from '../../image/svg/ReactIcon';
import Clock from "../Clock"
import Weather from "../Weather"

export default function Page(props){
  const {
    classes,
    // anchorEl,
    mobileMoreAnchorEl,
    // isMenuOpen,
    isMobileMenuOpen,
    // handleProfileMenuOpen,
    handleMobileMenuClose,
    // handleMenuClose,
    handleMobileMenuOpen,
    // logout,
    // views,
    baseClass,
    menuClass,
    handleDrawerOpen,
    title
  } = props
  
  // const menuId = 'primary-search-account-menu';
  // const renderMenu = (
  //   <Menu
  //     anchorEl={anchorEl}
  //     anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
  //     id={menuId}
  //     keepMounted
  //     transformOrigin={{ vertical: 'top', horizontal: 'right' }}
  //     open={isMenuOpen}
  //     onClose={handleMenuClose}
  //   >
  //     <MenuItem>
  //       <IconButton aria-label="show 4 new mails" color="inherit">
  //         <Badge badgeContent={4} color="secondary">
  //           <NotificationsIcon />
  //         </Badge>
  //       </IconButton>
  //       <IconButton aria-label="show 11 new notifications" color="inherit">
  //         <Badge badgeContent={11} color="secondary">
  //           <MailIcon />
  //         </Badge>
  //       </IconButton>
  //       <IconButton aria-label="" color="inherit">
  //         <Badge color="secondary">
  //           <BuildIcon />
  //         </Badge>
  //       </IconButton>
  //     </MenuItem>
  //     <MenuItem onClick={handleMenuClose}>Editar perfil</MenuItem>
  //     <MenuItem onClick={logout}>Cerrar sesión</MenuItem>
  //   </Menu>
  // );

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton aria-label="show 4 new mails" color="inherit">
          <Badge badgeContent={4} color="secondary">
            <MailIcon />
          </Badge>
        </IconButton>
        <p>Messages</p>
      </MenuItem>
      <MenuItem>
        <IconButton aria-label="show 11 new notifications" color="inherit">
          <Badge badgeContent={11} color="secondary">
            <NotificationsIcon />
          </Badge>
        </IconButton>
        <p>Notifications</p>
      </MenuItem>
      <MenuItem
        // onClick={handleProfileMenuOpen}
      >
        <IconButton
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  );

  return (
    <AppBar
      position="fixed"
      className={baseClass}
      // className={classes.grow}
    >
      <Toolbar>
        <IconButton
          edge="start"
          onClick={handleDrawerOpen}
          className={menuClass}
          // className={classes.menuButton}
          color="inherit"
          aria-label="open drawer"
        >
          <MenuIcon />
        </IconButton>
        <a
          title="Visite reactjs.org"
          href="https://reactjs.org/"
          target="_blank"
          rel="noopener noreferrer">
          <ReactIcon />
        </a>

        {/* <div className={classes.search}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            placeholder="Search…"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            inputProps={{ 'aria-label': 'search' }}
          />
        </div> */}
        <div className={classes.grow} />
        <Typography className={classes.title} variant="h6" noWrap>
          {title}
        </Typography>
        <div className={classes.sectionDesktop}>
          {/* <IconButton
            edge="end"
            aria-label="account of current user"
            aria-controls={menuId}
            aria-haspopup="true"
            onClick={handleProfileMenuOpen}
            // color="inherit"
          >
            <Badge badgeContent={15} color="secondary">
              <AccountCircle />
            </Badge>
          </IconButton> */}
          <Weather />
          <Clock />
        </div>
        <div className={classes.sectionMobile}>
          <IconButton
            aria-label="show more"
            aria-controls={mobileMenuId}
            aria-haspopup="true"
            onClick={handleMobileMenuOpen}
            color="inherit"
          >
            <MoreIcon />
          </IconButton>
        </div>
      </Toolbar>
      {renderMobileMenu}
      {/* {renderMenu} */}
    </AppBar>
  )
}