import React from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux'
// import { views } from '../../actions'
import Page from './page'

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  sectionDesktop: {
    display: 'none',
    marginLeft: '10px',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  typography: {
    padding: theme.spacing(2),
  },
  icon: {
    color: 'white'
  }
}));

const mapStateToProps = (state) => {
  return {
    userData: state.userData
  }
}

export default connect(mapStateToProps)(function PrimarySearchAppBar({ userData, baseClass, menuClass, handleDrawerOpen }) {
  const classes = useStyles();
  // const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  // const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  // function handleProfileMenuOpen(event) {
  //   setAnchorEl(event.currentTarget);
  // }

  function handleMobileMenuClose() {
    setMobileMoreAnchorEl(null);
  }

  // function handleMenuClose() {
  //   setAnchorEl(null);
  //   handleMobileMenuClose();
  // }

  function handleMobileMenuOpen(event) {
    setMobileMoreAnchorEl(event.currentTarget);
  }

  return (
    <Page
      title={userData ? userData.customer.name : ""}
      classes={classes}
      // anchorEl={anchorEl}
      mobileMoreAnchorEl={mobileMoreAnchorEl}
      // isMenuOpen={isMenuOpen}
      isMobileMenuOpen={isMobileMenuOpen}
      // handleProfileMenuOpen={handleProfileMenuOpen}
      handleMobileMenuClose={handleMobileMenuClose}
      // handleMenuClose={handleMenuClose}
      handleMobileMenuOpen={handleMobileMenuOpen}
      baseClass={baseClass}
      menuClass={menuClass}
      handleDrawerOpen={handleDrawerOpen}
      // iconsByViews={iconsByViews()}
    />
  );
})