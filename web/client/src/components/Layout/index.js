import React from 'react';
import clsx from 'clsx';
import { connect } from 'react-redux'
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Badge from '@material-ui/core/Badge';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
// import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import BrightnessHighIcon from '@material-ui/icons/BrightnessHigh'
import ToysIcon from '@material-ui/icons/Toys'
import EqualizerIcon from '@material-ui/icons/Equalizer';
import CloudIcon from '@material-ui/icons/Cloud'
import FilterVintageIcon from '@material-ui/icons/FilterVintage' 
import SettingsIcon from '@material-ui/icons/Settings'
import AccountCircle from '@material-ui/icons/AccountCircle'
import BuildIcon from '@material-ui/icons/Build';
import NotificationsIcon from '@material-ui/icons/Notifications'
import Collapse from '@material-ui/core/Collapse'
import MainToolBar from '../MainToolBar'
import View from "../View"
import { withRouter } from 'react-router-dom'
import { setLogIn } from '../../actions'

// import Clock from "../Clock"
// import Weather from "../Weather">


const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  appBarShiftFullClose: {
    marginLeft: 0,
    width: `calc(100% - ${0}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    backgroundColor: theme.palette.primary.main,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(8) + 1,
    },
  },
  drawerFullClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: 0
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    // padding: theme.spacing(3),
  },
  icon: {
    color: 'white'
  },
  listItem: {
    paddingLeft: '20px'
  },
  maskedLink: {
    textDecoration: 'none',
    color: theme.palette.text.primary
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

const mapStateToProps = (state) => {
  return {
    views: state.views,
    loggedIn: state.userData.loggedIn
  }
}

export default withRouter(connect(mapStateToProps, { setLogIn })(({/*views, */refreshCurrentView, setLogIn, loggedIn}) => {
  const DRAWER_FULL_CLOSED = 0, DRAWER_OPEN = 1, DRAWER_CLOSED = 2;
  function logout(){
    setLogIn(false);
    setAnchorEl(null);
    setOpen(DRAWER_FULL_CLOSED);
    // fetch('/logout')
    // .then(response => response.json())
    // .then(body => {
    //   if(body.isNotLoggedIn){
    //     setLogIn(false);
    //     setAnchorEl(null);
    //     setOpen(DRAWER_FULL_CLOSED);
    //   }
    // })
    // .catch(err => console.error(err));
  }
  
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(DRAWER_FULL_CLOSED);
  const [anchorEl, setAnchorEl] = React.useState(null);

  if(loggedIn && open === DRAWER_FULL_CLOSED){
    setOpen(DRAWER_CLOSED);
  }

  function handleDrawerOpen() {
    setOpen(DRAWER_OPEN);
  }

  function handleDrawerClose() {
    setOpen(DRAWER_CLOSED);
    handleProfileMenuClose();
  }
  function handleProfileMenuClose(){
    setAnchorEl(null);
  }
  function handleProfileMenuOpen(e){
    setAnchorEl(e.currentTarget);
    handleDrawerOpen();
  }

  function getIcon(view, key){
    let type;
    switch(view.linkTo){
      case 'thermometry':
        type = <BrightnessHighIcon />
        break
      case 'aeration':
        type = <ToysIcon />
        break
      case 'histogram':
        type = <EqualizerIcon />
        break
      case 'fillingUp':
        type = <FilterVintageIcon />
        break
      case 'weather':
        type = <CloudIcon />
        break
      case 'configurations':
        type = <SettingsIcon />
        break
      default:
        return
    }
    if (view.disabled) {
      return (
        <ListItem
          button
          key={key}
          disabled={true}
          className={classes.listItem}
        >
          <ListItemIcon>{type}</ListItemIcon>
          <ListItemText primary={view.title} />
        </ListItem>
      )
    }
    return (
      <Link
        to={`/${view.linkTo}`}
        className={classes.maskedLink}
        style={{
          textDecoration: 'none',
        }}
        key={key}
      >
        <ListItem
          button
          className={classes.listItem}
          onClick={handleDrawerClose}
        >
          <ListItemIcon className={open ? undefined : classes.icon}>{type}</ListItemIcon>
          <ListItemText primary={view.title} />
        </ListItem>
      </Link>

    )
  }
  let iconsByViews = []
 
  // fixed for demo
  let views = [
    { title: 'menu1', linkTo: 'thermometry' },
    { title: 'menu2', linkTo: 'aeration' },
    { title: 'menu3', linkTo: 'histogram' },
    { title: 'menu4', linkTo: 'fillingUp' },
    { title: 'menu5', linkTo: 'weather' },
    { title: 'menu6', linkTo: 'configurations'}
  ]

  for(var i in views){
    iconsByViews.push(getIcon(views[i], i))
  }
  
  const isMenuOpen = Boolean(anchorEl);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <MainToolBar
        baseClass={clsx(classes.appBar, {
          [classes.appBarShift]: open === DRAWER_OPEN,
          [classes.appBarShiftFullClose]: open === DRAWER_FULL_CLOSED
        })}
        menuClass={clsx(classes.menuButton, {
          [classes.hide]: open !== DRAWER_CLOSED,
        })}
        handleDrawerOpen={handleDrawerOpen}
      />
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerFullClose]: open === DRAWER_FULL_CLOSED,
          [classes.drawerOpen]: open === DRAWER_OPEN,
          [classes.drawerClose]: open === DRAWER_CLOSED,
        })}
        classes={{
          paper: clsx({
            [classes.drawerFullClose]: open === DRAWER_FULL_CLOSED,
            [classes.drawerOpen]: open === DRAWER_OPEN,
            [classes.drawerClose]: open === DRAWER_CLOSED,
          }),
        }}
        open={open === DRAWER_OPEN}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        <Divider />
        <List>
          
          {/* <IconButton
            edge="end"
            aria-label="account of current user"
            aria-controls={menuId}
            aria-haspopup="true"
            onClick={handleProfileMenuOpen}
            // color="inherit"
          >
            <Badge badgeContent={15} color="secondary">
              <AccountCircle />
            </Badge>
          </IconButton>
           */}
          <ListItem
            button
            className={classes.listItem}
            onClick={!isMenuOpen ? handleProfileMenuOpen : handleProfileMenuClose }
          >
            <ListItemIcon>
              <Badge badgeContent={15} color="secondary">
                <AccountCircle />
              </Badge>
            </ListItemIcon>
            <ListItemText primary={"Profile and more"}/>
          </ListItem>
          {/* {renderMenu} */}
          <Collapse in={isMenuOpen} timeout="auto" unmountOnExit>
            <List component="div"  className={classes.nested} disablePadding>
              {/* <ListItem button>
                <ListItemIcon>
                  <StarBorder />
                </ListItemIcon>
                <ListItemText primary="Starred" />
              </ListItem>
               */}
              <ListItem>
                <IconButton aria-label="show 4 new mails" color="inherit">
                  <Badge badgeContent={4} color="secondary">
                    <NotificationsIcon />
                  </Badge>
                </IconButton>
                <IconButton aria-label="show 11 new notifications" color="inherit">
                  <Badge badgeContent={11} color="secondary">
                    <MailIcon />
                  </Badge>
                </IconButton>
                <IconButton aria-label="" color="inherit">
                  <Badge color="secondary">
                    <BuildIcon />
                  </Badge>
                </IconButton>
              </ListItem>
              <ListItem button onClick={handleProfileMenuClose}>Edit profile</ListItem>
              <ListItem button onClick={logout}>Log out</ListItem>
            </List>
          </Collapse>
        </List>
        <Divider />
        <List>
          {iconsByViews}
        </List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <View
          refresh={refreshCurrentView}
        />
      </main>
    </div>
  );
}))
