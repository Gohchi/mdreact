import React from 'react'
import './CoverButton.css'

const CoverButton = props => (
  <div
    className="cover-button"
    onClick={props.onClick}
  >
    {props.message}
  </div>
)

export default CoverButton;