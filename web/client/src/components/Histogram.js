import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';
import { title } from '../config';
// import moment from 'moment';
// import momentLangEs from 'moment/locale/es.js';

// import excel from '../image/icon/excel.svg';

import './Histogram.css';
// import 'react-datepicker/dist/react-datepicker.css';

// moment.updateLocale('es', momentLangEs)

class Histogram extends Component {
  constructor(props){
    super(props)
    this.state = {
    }
  }
  render() {
    return (
      <DocumentTitle title={`${title} - Histogramas`}>
      <div className="view histogram">
        Histogram
      </div>
      </DocumentTitle>
    );
  }
}

export default Histogram;
