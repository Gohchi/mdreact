import React from 'react';

const TempMeter = props => {
  const { height, max } = props;
   let stepHeight = (height - 20) / 5;
  let textList = [];
  for(let i = 0; i < 5; i++) {
    const y = 20 + (stepHeight * i);
    textList.push(
      <text
        key={i}
        x="35"
        y={y+12}
      >
        {Math.round(max / 5 * (5-i))}{(i===0?"+":"")}
      </text>
    );
  }

  return (
    <svg
      className="temp-meter"
      style={{userSelect: 'none'}}
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
    >
      <defs>
        <linearGradient id="temp-meter-levels" x1="0%" y1="0%" x2="0%" y2="100%">
          <stop offset="0%" stopColor="#FF0000" stopOpacity="1" />
          <stop offset="25%" stopColor="#fc8f2f" stopOpacity="1" />
          <stop offset="50%" stopColor="#fff028" stopOpacity="1" />
          <stop offset="75%" stopColor="#20c931" stopOpacity="1" />
          <stop offset="100%" stopColor="#276af9" stopOpacity="1" />
        </linearGradient>
      </defs>
      <rect
        y="20"
        width="30"
        height={height - 20}
        fill="url(#temp-meter-levels)"
        strokeWidth="1"
        stroke="black"
      />
      <g
        fontFamily="Arial, Helvetica, sans-serif"
        fontSize="12"
      >
        <text x="6" y="15" fontWeight="bold" fontSize="18">C&deg;</text>
        {textList}
      </g>
      Sorry, your browser does not support inline SVG.  
    </svg>
  );
}
export default TempMeter;

/*
<svg class="temp-meter">
  <defs>
    <linearGradient id="grad1" x1="0%" y1="0%" x2="0%" y2="100%">
      <stop offset="0%" style="stop-color:#FF0000;stop-opacity:1;" />
      <stop offset="25%" style="stop-color:#fc8f2f;stop-opacity:1;" />
      <stop offset="50%" style="stop-color:#fff028;stop-opacity:1;" />
      <stop offset="75%" style="stop-color:#20c931;stop-opacity:1;" />
      <stop offset="100%" style="stop-color:#276af9;stop-opacity:1;" />
    </linearGradient>
  </defs>
  <rect
  	y="20"
    width="30"
    height="190"
    style="fill:url(#grad1);stroke-width:1;stroke:black;"
  />
  <g
    font-family="Arial, Helvetica, sans-serif"
    font-size="12"
  >
    <text x="6" y="15" font-weight="bold" font-size="18">C&deg;</text>
    <text x="35" y="30">50+</text>
    <text x="35" y="70">40</text>
    <text x="35" y="110">30</text>
    <text x="35" y="150">20</text>
    <text x="35" y="190">10</text>
  </g>
  Sorry, your browser does not support inline SVG.  
</svg>
 */