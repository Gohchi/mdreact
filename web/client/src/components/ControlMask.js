import React, { Component } from 'react';
import './ControlMask.css';

class ControlMask extends Component {
  constructor(props){
    super(props)
    this.state = {
    }
    // this.handleChange = this.handleChange.bind(this)
  }
  render() {
    const { left, top, centerTop, centerBottom, bottom, right } = this.props;
    let { labelLeft, labelTop, labelCenterTop, labelCenterBottom, labelBottom, labelRight } = this.props;
    
    labelLeft = labelLeft ? labelLeft : "LEFT";
    labelTop = labelTop ? labelTop : "TOP";
    labelCenterTop = labelCenterTop ? labelCenterTop : "CENTER TOP";
    labelCenterBottom = labelCenterBottom ? labelCenterBottom : "CENTER BOTTOM";
    labelBottom = labelBottom ? labelBottom : "BOTTOM";
    labelRight = labelRight ? labelRight : "RIGHT";

    return (
      <div className="cm grid-container">
        <div className="item2" title={labelLeft} onClick={left}><div className="content">{labelLeft}</div></div>
        <div className="item1" title={labelTop} onClick={top}><div className="content">{labelTop}</div></div>
        <div className="item3" title={labelCenterTop} onClick={centerTop}><div className="content">{labelCenterTop}</div></div>  
        <div className="item5" title={labelCenterBottom} onClick={centerBottom}><div className="content">{labelCenterBottom}</div></div>
        <div className="item6" title={labelBottom} onClick={bottom}><div className="content">{labelBottom}</div></div>
        <div className="item4" title={labelRight} onClick={right}><div className="content">{labelRight}</div></div>
      </div>
    );
  }
}

export default ControlMask;
