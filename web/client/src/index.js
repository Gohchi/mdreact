import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import './index.css'
import App from './App'
import registerServiceWorker from './registerServiceWorker'
import { BrowserRouter, Route } from "react-router-dom"

import store, { saveState } from './store'

class Root extends Component {
  componentDidMount(){
    window.addEventListener('unload', saveState)
  }
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <Route path="/" component={ App }/>
        </BrowserRouter>
      </Provider>
    )
  }
}

ReactDOM.render( 
  <Root />,
  document.getElementById('root')
);
registerServiceWorker();
