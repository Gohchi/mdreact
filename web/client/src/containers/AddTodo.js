import React from 'react'
import { connect } from 'react-redux'
import { addTodo, clearTodo } from '../actions'

let AddTodo = props => {
  let input
  
  return (
    <div>
      <form onSubmit={e => {
        e.preventDefault()
        if (!input.value.trim()) {
          return
        }
        props.addTodo(input.value)
        input.value = ''
      }}>
        <input ref={node => {
          input = node
        }} />
        <button type="submit">
          Añadir tarea
        </button>
        <button onClick={props.clearTodo}>
          Limpiar
        </button>
      </form>
    </div>
  )
}
AddTodo = connect(undefined, { addTodo, clearTodo })(AddTodo)

export default AddTodo