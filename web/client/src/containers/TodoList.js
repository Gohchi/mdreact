import React from 'react'
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
  return {
    todos: state.todos
  }
}

let TodoList = ({ todos }) => {
  if(!todos.length) return <div>empty</div>;
  return (
    <ul>
      {todos.map((o, i) => <li key={i}>({o.id}) {o.text}</li>)}
    </ul>
  )
}
TodoList = connect(mapStateToProps)(TodoList)

export default TodoList