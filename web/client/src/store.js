import { createStore, combineReducers, applyMiddleware } from 'redux';

import { mainMiddleware } from './middleware'
import todos from './reducers/todos'
import views from './reducers/views'
import userData from './reducers/userData'
import misc from './reducers/misc'

const reducer = combineReducers({
  todos,
  views,
  userData,
  misc
});

const customMiddleWare = store => next => action => {
  mainMiddleware(store, action);
  next(action);
}

// get state
const storeKey = 'GLOBAL_STATE'
const globalState = localStorage.getItem(storeKey)
const initialState = globalState ? JSON.parse(globalState) : undefined
const store = createStore(reducer, initialState, applyMiddleware(customMiddleWare))

export const saveState = () => {
  const state = store.getState()
  delete state.misc; // do not preserve misc data
  localStorage.setItem(storeKey, JSON.stringify(state))
}

export default store;