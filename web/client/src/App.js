import React, { Component } from 'react';
// import { Redirect } from 'react-router-dom';
import Main from './components/Layout'
// import logo from './logo.svg';
import Theme from "./Theme.js";
import { SnackbarProvider } from 'notistack';
import { connect } from 'react-redux'
import Notifier from './components/Notifier';
import { setUserData } from './actions'
import './App.css';

// import { token, host } from './config'

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      debug: false,
      backendMessage: null,
      reinitpopup: false,
      hideLoading: false,

      showShortcuts: false,
      currentKey: null,
      currentView: null,

      data: null,

      userData: null,
      showLogin: true,
      refreshCurrentView: false,
    }
    
    this.email = React.createRef()
    this.password = React.createRef()
    this.keyPressed = this.keyPressed.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleShow = this.handleShow.bind(this)
    
    // this.handleSubmit = this.handleSubmit.bind(this)
  }
  
  // Fetches our GET route from the Express server. (Note the route we are fetching matches the GET route from server.js
  // callBackendAPI = async () => {
  //     const response = await fetch('/express_backend')
  //       .catch((err) => console.error(err));
  //       //   console.log(error);
  //       // });
  //     const body = await response.json();

  //     if (response.status !== 200) {
  //       // throw Error(body.message) 
  //       console.error(body.message)
  //     }
  //     return body;
  // }
  // checkSession = async () => {
  //   const response = await fetch('/session');
  //   if (response.status === 401) {
  //     this.openLogin();
  //   }
  //   const body = await response.json();

  //   // if (response.status !== 200) {
  //   //   throw Error(body.message) 
  //   // }
  //   return body;
  // }
  keyPressed(event) { 
    let state = {
      currentKey: event.keyCode
    }
    if(this.state.currentKey === 17 && event.keyCode > 48 && event.keyCode < 57) this.changeView(event.keyCode - 49)
    state.showShortcuts = (event.keyCode === 17 && !this.state.showShortcuts)
    this.setState(state)
  }
  changeView(index){
    let view;
    switch (index) {
      case 1:
        view = 'aeration';
        break;
      case 2:
        view = 'histogram';
        break;
      case 3:
        view = 'fillingUp';
        break;
      case 4:
        view = 'weather';
        break;
      case 5:
        view = 'configurations';
        break;
      case 6:
        view = 'announcements';
        break;
      case 7:
        view = 'support';
        break;
    
      default:
        view = 'thermometry';
        break;
    }
    this.setState({
      currentView: view
    })
    // if(!window.location.href.endsWith(view))
    //   window.location.href = view;
  }
  
  handleClose() {
    this.setState({ showLogin: false });
  }


  handleShow() {
    this.setState({ showLogin: true });
  }
  
  // doLogin = async (data) => {
  //   const response = await fetch('/login', {
  //     method: 'post',
  //     headers: {
  //         'Accept': 'application/json',
  //         'Content-Type': 'application/json',
  //     },
  //     // mode: "cors",
  //     body: JSON.stringify({
  //       "email": data.email,
  //       "password": data.password
  //     })
  //   })
  //   const body = await response.json()
  //   // if (this.password.current && this.password.current.value) this.password.current.value = "";

  //   if (response.status !== 200) {
  //     // throw Error(body.message) 
  //     this.setState({
  //       backendMessage: body.message
  //     })
  //   }
  //   return body;
  // }
  // handleSubmit(event){ 
  //   event.preventDefault();
  //   this.setState({
  //     showLogin: false,
  //     hideLoading: false
  //   })
  //   const data = new FormData(event.target);
    
  //   this.doLogin({ email: data.get('email'), password: data.get('password') })
  //     .then(res => {
  //       if(res.isNotLoggedIn){
  //         this.setState({
  //           showLogin: true,
  //           hideLoading: true
  //         })
  //       }else{
  //         this.props.setUserData(res.user)
  //         this.setState({
  //           userData: res.user,
  //           hideLoading: true,
  //           refreshCurrentView: !this.state.refreshCurrentView
  //         })
  //         this.callBackendAPI()
  //           .then(res => this.setState({ data: res.express }))
  //           .catch(err => console.error(err));
  //       }
  //     })
  //     .catch(err => console.error(err));
  // }
  UNSAFE_componentWillMount() {
    // BannerDataStore.addChangeListener(this._onchange);
    document.addEventListener("keydown", this.keyPressed, false);
  }
  componentWillUnmount() {
    // BannerDataStore.removeChangeListener(this._onchange);
     document.removeEventListener("keydown", this.keyPressed, false);
  }
  componentDidMount(){
      // Call our fetch function below once the component mounts
      // this.callBackendAPI()
      // .then(res => this.setState({ data: res.express }))
      // .catch(err => console.error(err));

      // this.checkSession()
      //   .then(res => {
      //     if (res.isNotLoggedIn) {
      //       this.setState({
      //         showLogin: true,
      //         hideLoading: true
      //       })
      //     } else {
      //       this.props.setUserData(res.user)
      //       this.setState({
      //         userData: res.user,
      //         hideLoading: true
      //       })
      //     }
      //   })
      //   .catch(err => console.error(err));
  }

  render() {
    let { refreshCurrentView } = this.state;
    const { isLoading } = this.props;
    return (
      <Theme>
        <SnackbarProvider>
        <Notifier />
        <Main
          refreshCurrentView={refreshCurrentView}
          login={{}}
        />
        <div className="main-container">
          <div className={"message" + (this.state.reinitpopup?"-hidden":"")}>
            {this.state.backendMessage}
          </div>
          <div className={"loading" + (!isLoading?"-hidden":"")}>
            <div className="spinner"></div>
          </div>
          <div>
            <div className={"temp" + (this.state.data ? " connected" : "")} style={{ display: this.state.debug ? '' : 'none'}}>{this.state.data || "Backend: disconnected"}</div>
          </div>
        </div>
        </SnackbarProvider>
      </Theme>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.misc.isLoading
  }
}
export default connect(mapStateToProps, { setUserData })(App);