import { ADD_TODO, TOGGLE_TODO, CLEAR_TODO } from '../actions'

const todo = (state = {}, action) => {
  switch (action.type) {
    case ADD_TODO:
      return {
        id: action.id,
        text: action.text,
        completed: false
      }
    case TOGGLE_TODO:
      if (state.id !== action.id) {
        return state
      }

      return Object.assign({}, state, {
        completed: !state.completed
      })

    default:
      return state
  }
}

const todos = (state = [], action) => {
  switch (action.type) {
    case ADD_TODO:
      let id =  Math.max.apply(Math, [{id:0}].concat(state).map(function(o) { return o.id || 0; })) + 1;
      return [
        ...state,
        todo(undefined, Object.assign(action, { id }))
      ]
    case TOGGLE_TODO:
      return state.map(t =>
        todo(t, action)
      )
    case CLEAR_TODO:
      return []
    default:
      return state
  }
}

export default todos