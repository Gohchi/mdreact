import { SET_LOADING, SET_LOGIN_ERROR, ENQUEUE_SNACKBAR, CLOSE_SNACKBAR, REMOVE_SNACKBAR } from '../actions'

const defaultState = {
  notifications: [],
  isLoading: false
};

export default (state = defaultState, action) => {
  switch (action.type) {
    // loading
    case SET_LOADING:
      return Object.assign({}, state, {
        isLoading: action.isLoading
      })
    case SET_LOGIN_ERROR:
      return Object.assign({}, state,
        action.hasError ? { loginError: action.message } : { loginError: undefined }
      )
      
    // snack notifications
    case ENQUEUE_SNACKBAR:
        return {
            ...state,
            notifications: [
                ...state.notifications,
                {
                    key: action.key,
                    ...action.notification,
                },
            ],
        };

    case CLOSE_SNACKBAR:
        return {
            ...state,
            notifications: state.notifications.map(notification => (
                (action.dismissAll || notification.key === action.key)
                    ? { ...notification, dismissed: true }
                    : { ...notification }
            )),
        }

    case REMOVE_SNACKBAR:
        return {
            ...state,
            notifications: state.notifications.filter(
                notification => notification.key !== action.key,
            ),
        };
    default:
      return state
  }
}