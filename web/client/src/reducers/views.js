import { GET_VIEWS, ADD_VIEWS } from '../actions'

const views = (state = [], action) => {
  switch (action.type) {
    case GET_VIEWS:
      return state
    case ADD_VIEWS:
        // if(state.length) return
      return action.items
    default:
      return state
  }
}

export default views