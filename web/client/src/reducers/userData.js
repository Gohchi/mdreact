import { SET_USER_DATA, SET_LOG_IN } from '../actions'

const defaultState = {
  customer: { name: '' },
  loggedIn: false,
  authToken: ''
};

const userData = (state = defaultState, action) => {
  switch (action.type) {
    case SET_USER_DATA:
      return Object.assign({}, state, action.data);
    case SET_LOG_IN:
        return Object.assign({}, state, {
          loggedIn: action.loggedIn,
          authToken: action.authToken || ''
        })
    default:
      return state
  }
}

export default userData