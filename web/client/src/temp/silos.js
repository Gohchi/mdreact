const silos = [
  [
    {
      radius: 90
    },
    {
      radius: 75,
      cables: Array(8)
    },
    {
      radius: 50,
      cables: Array(3)
    },
    {
      radius: 0,
      cables: Array(0)
    }
  ],
  [
    {
      radius: 120,
      cables: []
    },
    {
      radius: 95,
      cables: Array(18)
    },
    {
      radius: 60,
      cables: Array(14)
    },
    {
      radius: 30,
      cables: Array(7)
    },
    {
      radius: 0,
      cables: Array(1)
    }
  ]
]

export { silos }