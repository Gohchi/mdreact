import React from 'react';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#8cbaff',
    },
    secondary: {
      main: '#f44336',
    },
    other: {
      main: '#87abdb',
    }
  },
});

export default function Theme(props){
  return (
    <ThemeProvider theme={theme}>
      {props.children}
    </ThemeProvider>
  )
}
