const Plant = require('../controllers/plant');

module.exports = api => {
  api.route('/plants')
    .get(Plant.get)
    .post(Plant.post);
};

