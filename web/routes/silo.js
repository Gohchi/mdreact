const controller = require('../controllers/silo');

module.exports = api => {
  api.route('/silo')
    .delete(controller.delete)
    .post(controller.post);
    
  api.route('/silos/positions')
    .post(controller.updatePositions);
};

