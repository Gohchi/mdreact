const User = require('../controllers/user');

module.exports = api => {
  api.route('/users')
    .get(User.get)
    .post(User.post);
};

