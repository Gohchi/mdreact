const express = require('express');
const mongoose = require('mongoose');
const Customer = require('../models/customer');
const Plant = require('../models/plant');
const Silo = require('../models/silo');
const Tem = require('../models/tem');
const fs = require('fs');
const pug = require('pug');
const Configuration = require('../models/plant.configuration');

const router = express.Router();

mongoose.connect('mongodb://localhost/testing', { useNewUrlParser: true });

var multer = require('multer');
var upload = multer({ dest: 'uploads/' })

router.all('*', (req, res, next) => {
  if(req.path.substring(0, 6) == '/debug'){
    next();
    return;
  }
  var date = getDate();
  var msg = `${date} ${req.method}: ${req.path}`;
  var tryLogIn = req.method == 'POST' && req.path == '/login';
  var isLoggedIn = req.session.user && req.cookies.user_sid;
  if (isLoggedIn || tryLogIn)  {
    console.log(`${msg}${(isLoggedIn ? ` user: ${req.session.user.name} (${req.cookies.user_sid.substring(0, 10) + '...'})` : '')}`);
    next();
  } else {
    console.log(`${msg} DENIED`);
    res.status(401).send({ message: 'you shall not pass', isNotLoggedIn: true });
  }
});
require('./user')(router);
require('./plant')(router);
require('./silo')(router);
require('./session')(router);
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', () => {
  console.log('DB connected!');
});

function getDetConfig(buffer){
  var _offset = 0,
  fileSize = buffer.length,
  config = [];
  function getBytes(bytes, fix){
    let data = buffer.slice(_offset, _offset += bytes);
    if (fix) _offset++;
    return data;
  }
  function getInt(){
    return getBytes(2).readInt8();
  }
  function getBool(){
    return getBytes(1, true)[0] === 1;
  }
  function getLong(){
    return getBytes(4).readInt8();
  }
  function getListWith(dataFn){
    let list = [];
    for(let i = 0; i < 37; i++)
      list.push(dataFn());
    return list;
  }
  while (_offset < fileSize) {
    let record = {
      LM35: getListWith(getLong.bind(this)),
      JRef: getListWith(getLong.bind(this)),
      Retor: getListWith(getLong.bind(this)),
      ManAB: getListWith(getInt.bind(this)),
      Verde: getListWith(getBool.bind(this)),
      MalRet: getListWith(getBool.bind(this))
    };
    config.push(record);
  }
  // console.log('_offset', _offset, 'fileSize', fileSize);
  return config;
}
function getResConfig(buffer){
  var _offset = 0,
  fileSize = buffer.length,
  config = [];
  function getBytes(bytes, fix){
    let data = buffer.slice(_offset, _offset += bytes);
    if (fix) _offset++;
    return data;
  }
  function getInt(){
    return getBytes(2).readInt8();
  }
  while (_offset < fileSize) {
    let list = [];
    for(let i = 0; i < 37; i++){
      list.push(getInt());
    }
    config.push(list);
  }
  // console.log('_offset', _offset, 'fileSize', fileSize);
  return config;
}
function getTemOrIncConfig(buffer){
  var _offset = 0,
  fileSize = buffer.length,
  config = [];
  function getBytes(bytes, fix){
    let data = buffer.slice(_offset, _offset += bytes);
    if (fix) _offset++;
    return data;
  }
  function getByte(){
    return getBytes(1)[0];
  }
  while (_offset < fileSize) {
    let list = [];
    for(let i = 0; i < 37; i++){
      list.push(getByte());
    }
    config.push(list);
  }
  return config;
}
function getCabConfig(buffer){
  var _offset = 0,
  fileSize = buffer.length,
  config = [];
  function getBytes(bytes, fix){
    let data = buffer.slice(_offset, _offset += bytes);
    if (fix) _offset++;
    return data;
  }
  function getByte(){
    return getBytes(1)[0];
  }
  while (_offset < fileSize) {
    let cableD = [];
    for(let i = 0; i < 25; i++){
      let cDia = [];
      for(let l = 0; l < 35; l++){
        cDia.push({
          CDia: getByte()
        });
      }
      cableD.push({
        CableD: cDia
      });
    }
    config.push(cableD);
  }
  // console.log('_offset', _offset, 'fileSize', fileSize);
  return config;
}
function getFcbConfig(buffer){
  var _offset = 0,
    maxCables,
    fileSize = buffer.length,
    config = [];

  if(fileSize % 341 == 0 && !(fileSize % 581 == 0 && tryFileControlBlock(45)) && !(fileSize % 1121 == 0 && tryFileControlBlock(90))) // fcb record with maxCables = 25
  {
    _offset = 0;
    while (_offset < fileSize)
      config.push(FileControlBlock(25));
  }
  else if (fileSize % 581 == 0 && !(fileSize % 1121 == 0 && tryFileControlBlock(90))) // fcb record with maxCables = 45
  {
    _offset = 0;
    while (_offset < fileSize)
      config.push(FileControlBlock(45));
  }
  else if (fileSize % 1121 == 0) // fcb record with maxCables = 90
  {
    _offset = 0;
    while (_offset < fileSize)
      config.push(FileControlBlock(90));
  }
  function getBytes(bytes, fix){
    let data = buffer.slice(_offset, _offset += bytes);
    if (fix) _offset++;
    return data;
  }
  function getInt(){
    return getBytes(2).readInt8();
  }
  function getLong(){
    return getBytes(4).readInt8();
  }
  function getByte(){
    return getBytes(1)[0];
  }
  function getBool(){
    return getBytes(1, true)[0] === 1;
  }
  function getString(len){
    return getBytes(len).toString().trim();
  }
  function tryFileControlBlock(maxCables){
    _offset = 0;
    try
    {
      while (_offset < fileSize)
      {
        FileControlBlock(maxCables);
      }
    }
    catch (Exception)
    {
      return false;
    }
    return true;
  }
  function FileControlBlock(maxCables){
    return {
      NombrePlanta: getString(15),
      SiloFil: getString(8),
      NumFila: getInt(),
      CantCables: getByte(),
      CantTc: getBytes(maxCables),
      NomCables: getBytes(maxCables),
      TcInicio: getInt(),
      TcFin: getInt(),
      TabOfsetCab: getInt(),
      TablerSufix: getString(1),
      MedTriple: getByte(),
      NumPlanta: getByte(),
      RecNumFcb: getInt(),
      AmbDia: getByte(),
      AmbNoche: getByte(),
      MandoAB: getBytes(maxCables),
      OffsetTc: getBytes(maxCables),
      RecNumTem: (function(){
        let list = [];
        for(let i = 0; i < maxCables; i++){
          list.push(getLong());
        }
        return list;
      })(),
      RecPrimTcMid: (function(){
        let list = [];
        for(let i = 0; i < maxCables; i++){
          list.push(getInt());
        }
        return list;
      })(),
      RecUltTcMid: (function(){
        let list = [];
        for(let i = 0; i < maxCables; i++){
          list.push(getInt());
        }
        return list;
      })(),
      NumMid: getInt()
    };
  }
  return config;
}
function getCnfConfig(buffer){
  var _offset = 0;
  var pathNameLength, //45, 80
    hasScale, hasAeration; 
  switch (buffer.length){
    case 695:
      pathNameLength = 45;
      hasScale = true;
      hasAeration = true;
      break;
    case 659:
      pathNameLength = 45;
      hasScale = true;
      hasAeration = false;
      break;
    case 686:
      pathNameLength = 45;
      hasScale = false;
      hasAeration = true;
      break;
    case 650:
      pathNameLength = 45;
      hasScale = false;
      hasAeration = false;
      break;
    case 730:
      pathNameLength = 80;
      hasScale = true;
      hasAeration = true;
      break;
    case 694:
      pathNameLength = 80;
      hasScale = true;
      hasAeration = false;
      break;
    case 721:
      pathNameLength = 80;
      hasScale = false;
      hasAeration = true;
      break;
    case 685:
      pathNameLength = 80;
      hasScale = false;
      hasAeration = false;
      break;
  }
  function getBytes(bytes, fix){
    let data = buffer.slice(_offset, _offset += bytes);
    if (fix) _offset++;
    return data;
  }
  function getInt(){
    // return (((bytes[0] & 0xFF) << 8) | (bytes[1] & 0xFF));
    return getBytes(2).readInt8();
  }
  function getByte(){
    return getBytes(1)[0];
  }
  function getBool(){
    return getBytes(1, true)[0] === 1;
  }
  function getString(len){
    return getBytes(len).toString().trim();
  }
  var empresa = getString(25);
  var programa = getString(8);
  var pathName = getString(pathNameLength);

  // // debug
  // for(let i = 0; i < 100; i++){
  //   console.log(`${i}:`, getInt(buffer.slice(_offset + i, _offset + i + 2)));
  // }

  var tablero = [];
  for(let i = 0; i < 3; i++){
    let tab = {};
    if (hasScale)
    {
      tab = {
        BalanPromedio: getByte(),
        BalanEnab: getBool()
      };
    }
    tab = Object.assign(tab, {
			Puerto: getByte(),
			Enabled: getBool(),
      PrimFila: getInt(),
      UltFila: getInt(),
      PrimTc: getInt(),
      UltTc: getInt(),
      TiMing: getBytes(6),
      CantRemotosNAI: getByte(),
      Circuito: (function(){
        let list = [];
        for(let j = 0; j < 4; j++){
          list.push({
            PrimFila: getInt(),
            UltFila: getInt(),
            PrimTc: getInt(),
            UltTc: getInt()
          });
        }
        return list;
      })()
    });
    tablero.push(tab)
  }
  var temperaTope = getInt();
  var incremenTope =  getInt();
  var cantMid =  getInt();
  var puntosMid =  (function(){
    let list = [];
    for(let i = 0; i < 100; i++){
      list.push(getInt());
    }
    return list;
  })();
  var tiempoResis =  getInt();
  var tiempoTemper =  getInt();
  var tiempoCorto =  getInt();
  var numred =  getInt();
  var medContinua = getBool();
  var reiteraMax =  getInt();
  var vecesJref =  getInt();
  var timeoutPC =  getInt();
  var midEnabled =  (function(){
    let list = [];
    for(let i = 0; i < 100; i++){
      list.push(getBool());
    }
    return list;
  })();
  
  var midAirEnabled,
    cantMidAir,
    cantProgVent,
    intervaloAir;
  if (hasAeration){
    midAirEnabled = (function(){
      let list = [];
      for(let i = 0; i < 15; i++){
        list.push(getBool());
      }
      return list;
    })();
    cantMidAir = getInt();
    cantProgVent = getInt();
    intervaloAir = getInt();
  }
  return {
    Empresa: empresa,
    Programa: programa,
    PathName: pathName,
    Tablero: tablero,
    TemperaTope: temperaTope,
    IncremenTope: incremenTope,
    CantMid: cantMid,
    PuntosMid: puntosMid,
    TiempoResis: tiempoResis,
    TiempoTemper: tiempoTemper,
    TiempoCorto: tiempoCorto,
    Numred: numred,
    MedContinua: medContinua,
    ReiteraMax: reiteraMax,
    VecesJref: vecesJref,
    TimeoutPC: timeoutPC,
    MIDEnabled: midEnabled,
    MidAirEnabled: midAirEnabled,
    CantMidAir: cantMidAir,
    CantProgVent: cantProgVent,
    IntervaloAir: intervaloAir,
    EndReached: _offset == buffer.length
  };
}


// const compiledFunction = pug.compileFile('cnf.pug');
// router.get('/debug/read_cnf', (req, res) => {
//   binary = fs.readFileSync('./sclara.cnf');
//   // console.log(binary.slice(0, 25));
//   var config = getCnfConfig(binary);
//   // console.log(config);
//   res.send(compiledFunction(config));
// });

router.get('/debug/testconfig', (req, res) => {
  let customerId = "5bfdcc65637cbb2138310de4" //"5c16d6309736bf46e0e676c7" //
    // req.session.user.customer.id;
  Configuration.find()
    .populate({
      path: 'plant',
      populate: {
        path: 'customer',
        // match: {
        //   id: customerId
        // }
        // model: 'Customer'
      }
      // match: {
      //   customer: customerId
      // }
    })
    .exec((err, configs) => {
      //{ customer: customerId }
      let list = [];
      configs = configs.filter(config => {
        return config.plant.customer.id === customerId;
      });
      configs.forEach(config =>
        list.push(Object.assign({
          name: config.plant.name,
          cnfLoaded: config.cnfLoaded,
          allLoaded: config.allLoaded
        }))
      )
  
      res.send({
        plants: list
      })
    })
});
router.post('/upload/cnf', upload.any(), (req, res) => {
  var file = req.files[0];
  var binary = fs.readFileSync(file.path);
  var config = getCnfConfig(binary);
  console.log(config);
  res.send(config);
  // res.send({ response: "ok" });
  
  // res.send(compiledFunction(config));
});

function getExtension(filename){
  return /\.([A-Za-z0-9]+)$/g.exec(filename)[1].toLowerCase()
}
function getBinaryFor(type, files){
  let file = files.filter(o => getExtension(o.originalname) === type)[0];
  let binary = fs.readFileSync(file.path);
  //fs.unlink(file.path);
  return binary;
}

function getDateFromVB6(double){
	var baseDate = new Date(1899, 11, 30);
	baseDate.setDate(baseDate.getDate() + parseInt(double));
	var time = double - parseFloat(parseInt(double));
	//baseDate.setHours(parseInt(time * 24));
	//baseDate.setMinutes(parseInt(time * 24 * 60));
	baseDate.setSeconds(parseInt(time * 24 * 60 * 60));
	
	return baseDate;	
}
function getDiaConfig(buffer){
  
  var _offset = 0,
    fileSize = buffer.length,
    config = [];
  function getBytes(bytes, fix){
    let data = buffer.slice(_offset, _offset += bytes);
    if (fix) _offset++;
    return data;
  }
  function getDouble(){
    return getBytes(8).readDoubleLE(0);
  }
  
  while (_offset < fileSize)
  {
    config.push(getDateFromVB6(getDouble()));
  }
  return config;
}
router.post('/getvb6date', upload.any(), (req, res) => {
  let config = getDiaConfig(getBinaryFor("dia", req.files));
  console.log(config.length);
});
router.get('/silo/get/temperature', upload.any(), (req, res) => {
  let siloNumber = parseInt(req.query.silo);
  Silo.findOne({ plant: req.query.plantId, siloNumber: siloNumber }, (err, silo) => {
    if(silo){
      Tem.findOne({ silo: silo._id }, (err, tem) => {
        let st = { rafters: [{ cables: [] }] };
        let tcIndex = 0;
        silo.rafters.forEach((r, ri) => {
          if(ri === 0) {
            st.rafters[0].radius = r.radius;
            return;
          }
          let nr = { cables: [], radius: r.radius };
          r.cables.forEach(c => {
            let nc = { tcs: [], name: c.name, angle: c.angle };
            for(let i = 0; i < c.tcLength; i++){
              let temperatures = tem.temperatureByTC[tcIndex++];
              if(req.query.fix) {
                temperatures.pop();
                temperatures.shift();
              }
              nc.tcs.push(temperatures);
            }
            nr.cables.push(nc);
          });
          st.rafters.push(nr);
        });
        let dates = tem.dates.slice();
        if(req.query.fix){
          dates.pop();
          dates.shift();
        }
        res.send({
          siloTems: st,
          dates: dates,
          silo: tem.silo,
          temperatureByTC: tem.temperatureByTC
        });
        return;
        if(tem){
          console.log(tem.temperatureByTC.length);
        }
      });
    }
  });  
});
router.get('/silo/get/temperature/summary', upload.any(), (req, res) => {
  Silo.find({ plant: req.query.plantId }, (err, silos) => {
    let ids = silos.map(s => s._id);
    // { "fb" : { id: { $in : arrayOfIds } }
    // Tem.find({ silo: silo._id }, (err, tem) => {
    Tem.find({ silo: { $in : ids } })
      .populate('silo')
      .exec((err, tem) => {
      let lastDate, lastDateIndex;
      let returnData = tem.map(t => {
        if(!lastDate){
          lastDate = new Date(Math.max.apply(null, t.dates.map(function(e) {
            return new Date(e);
          })));
          lastDateIndex = t.dates.map(d => d.toLocaleDateString()).indexOf(lastDate.toLocaleDateString())
        }

        let lastTemperature = t.temperatureByTC.map(o => o[lastDateIndex])

        return {
          siloNumber: t.silo.siloNumber,
          temperatureByTC: lastTemperature
        }
      })
      
      res.send({
        date: lastDate,
        data: returnData
      });
    });
  });  
});
router.get('/silo/get/real/tem', upload.any(), (req, res) => {
  let siloNumber = parseInt(req.query.silo);
  Silo.findOne({ plant: req.query.plantId, siloNumber: siloNumber }, (err, silo) => {
    if(silo){
      Tem.findOne({ silo: silo._id }, (err, tem) => {
        res.send({
          dates: tem.dates,
          silo: tem.silo,
          temperatureByTC: tem.temperatureByTC
        });
        return;
        if(tem){
          console.log(tem.temperatureByTC.length);
          // Silo.find({
          //   plant: req.query.plantId,
          //   //siloNumber: parseInt(req.query.silo)
          // }, (err, silos) => {
          //   // (function(obj){
          //   //   var sum = 0;
          //   //   for (var i = 0; i < obj.length; i++){
          //   //     sum += obj[i].CantTc.reduce(function(t, n) { return t + n; })
          //   //   }
          //   //   return sum;
          //   // })(config)
          //   let tcToSkip = 0;
          //   let i = 1;
          //   for(; i < siloNumber; i++){
          //     let siloRound = silos.filter(s => s.siloNumber === i)[0];
          //     // console.log(siloRound);
          //     tcToSkip += siloRound.rafters.reduce((total, rafter) => total + rafter.cables.reduce((t, n) => t + n.tcLength, 0), 0);
          //   }
            
          //   let silo = silos.filter(s => s.siloNumber === i)[0];
          //   tcUntil = silo.rafters.reduce((total, rafter) => total + rafter.cables.reduce((t, n) => t + n.tcLength, 0), 0);
          //   let tcs = tem.temperatureByTC.slice(tcToSkip, tcToSkip + tcUntil);
          //   // console.log(silo)
          //   res.send({ tcs, dates: tem.dates });
          // });
        }
      });
    }
  });
});
router.post('/upload/tem', upload.any(), (req, res) => {
  let records = getTemOrIncConfig(getBinaryFor("tem", req.files));
  let dates = getDiaConfig(getBinaryFor("dia", req.files));
  Silo
    .find({ plant: req.body.plantId })
    .select('-__v').exec((err, silos) => {
    if (err) console.log(err);
    
    let tcToSkip = 0;
    silos.sort(function(n,c){
      return c.siloNumber > n.siloNumber ? 1 : -1;
    }).forEach((silo) => {
      tcUntil = silo.rafters.reduce((total, rafter) => total + rafter.cables.reduce((t, n) => t + n.tcLength, 0), 0);
      let tcs = records.slice(tcToSkip, tcToSkip + tcUntil);

      // Create or Update
      Tem.findOne({ silo: silo._id }, (err, tems) => { 
        if(tems !== null) {
          dates.forEach((date, i) => {
            let dateIndex = tems.dates.map(Number).indexOf(+date);
            // date exists
            if(dateIndex >= 0) {
              // find
              tems.temperatureByTC = tems.temperatureByTC.map((r, ti) => {
                let l = r.slice();
                l[dateIndex] = tcs[ti][i];
                return l;
              });
            } else {
              let indexToAddDate = dates.reduce((total, value) => {
                return total + (value < date ? 1 : 0)
              }, 0);
              dates.splice(indexToAddDate, 0, date);
            }
          })
          tems.save();
        } else {
          (
            new Tem({
              silo: silo._id,
              temperatureByTC: tcs,
              dates: dates
            })
          ).save()
        }
      });
      tcToSkip += tcUntil;
    });
    res.send({ response: "tem uploaded" });
  });
});

router.post('/upload/fcb', upload.any(), (req, res) => {
  let records = getFcbConfig(getBinaryFor("fcb", req.files));
  records.forEach((record, index) => {
    const type = !!~record.SiloFil.toLowerCase().indexOf("silo") ? "silo" : "cell";
    if (type === 'silo') {
      let groupByTCLength = record.CantTc.reduce((groups, item) => {
        if (item) { // ignore zeroes
          groups[item.toString()] = groups[item.toString()] !== undefined ? groups[item.toString()] + 1 : 1;
        }
        return groups;
      }, {});

      const ringCount = Object.keys(groupByTCLength).length + 1;
      let rafters = [{
        radius: ringCount * 30,
        cables: []
      }];

      let cableName = 1;
      for(let cableType in groupByTCLength) {
        const cableCount = groupByTCLength[cableType];
        const cables = [];
        for(let i = 0; i < cableCount; i++){
          cables.push({
            name: cableName++,
            angle: 360 / cableCount * i,
            tcLength: parseInt(cableType)
          });
        }
        rafters.push({
          radius: ringCount === rafters.length + 1 && cables.length === 1
            ? 0
            : (ringCount - rafters.length) * 30,
          cables: cables
        })
        // console.log(cableType)
      }
      let silo = new Silo({
        name: `${record.SiloFil}`,
        type: type,
        plant: req.body.plantId,
        siloNumber: record.NumFila,
        rafters: rafters,
        TcStart: record.TcInicio,
        TcEnd: record.TcFin,
        x: index * 100,
        y: 100
      });
      
      silo.save((err, newSilo) => {
        if (err) return console.error(err);
      });
    }
  });
  res.send({ response: "fcb uploaded" });
  // Configuration.findOne({ plant: req.body.plantId }, (err, config) => {
})
router.post('/upload/config_files', upload.any(), (req, res) => {
  Configuration.findOne({ plant: req.body.plantId }, (err, config) => {
    if(!config.cnfConfig) {   //load CNF
      config.cnfConfig = getCnfConfig(getBinaryFor("cnf", req.files));
    }
    if(!config.fcbConfig || !config.fcbConfig.length) {   //load FCB
      config.fcbConfig = getFcbConfig(getBinaryFor("fcb", req.files));
    }
    // // if(!config.cabConfig || !config.cabConfig.length) {   //load CAB
    // //   config.cabConfig = getCabConfig(getBinaryFor("cab", req.files));
    // // }
    
    if(!config.temConfig || !config.temConfig.length) {   //load TEM
      config.temConfig = getTemOrIncConfig(getBinaryFor("tem", req.files));
    }
    if(!config.resConfig || !config.resConfig.length) {   //load RES
      config.resConfig = getResConfig(getBinaryFor("res", req.files));
    }
    if(!config.detConfig || !config.detConfig.length) {   //load DET
      config.detConfig = getDetConfig(getBinaryFor("det", req.files));
    }
    if(!config.incConfig || !config.incConfig.length) {   //load INC
      config.incConfig = getTemOrIncConfig(getBinaryFor("inc", req.files));
    }
    
    config.save();
    // clean
    req.files.forEach(file => {
      fs.unlink(file.path,  fsErr => { if (fsErr) console.log(fsErr) });
    });
    res.send({ response: "ok" });
  });
  // res.send(compiledFunction(config));
});

// create a GET route
router.get('/express_backend', (req, res) => {
  res.send({ express: 'Backend: connected' });
});

router.get('/toolbar/menuitems', (req, res) => {
  Plant.find({ customer: req.session.user.customer.id }, (err, plants) => {
    if (err) console.log(err);
    // Configuration.find({ plant: { $in: plants.map(o => o._id) }, allLoaded: true }, (err2, configs) => {
    //   if(configs.length){
        res.send({menuItems: [
          { title: "menu1", label: "temperature", symbol: "🌡️", linkTo: "menu1" },
          { title: "menu2", label: "air", symbol: "💨", linkTo: "menu2", disabled: false },
          { title: "menu3", label: "chart", symbol: "📈", linkTo: "menu3", disabled: false },
          { title: "menu4", label: "seed", symbol: "🌱", linkTo: "menu4", disabled: false },
          { title: "menu5", label: "weather", symbol: "⛅", linkTo: "menu5", disabled: false },
          { title: "menu6", label: "gear", symbol: "⚙️", linkTo: "menu6", disabled: false },
          { title: "menu7", label: "bell", symbol: "🔔", linkTo: "menu7", disabled: false },
          { title: "menu8", label: "wrench", symbol: "🔧", linkTo: "menu8", disabled: false }
        ]});
    //   }else{
    //     res.send({menuItems: [
    //       { title: "Configuraciones", label: "gear", symbol: "⚙️", linkTo: "configurations" },
    //     ]});
    //   }
    // })
  })
});

function getDate(){
  var date = new Date();
  var pad = value => value.toString().padStart(2, 0);
  return `${date.getFullYear().toString().substring(2,4)}/${pad(date.getMonth())}/${pad(date.getDate())} ${pad(date.getHours())}:${pad(date.getMinutes())}:${pad(date.getSeconds())}`;
}


router.get('/silos', (req, res) => {
  Plant.find({ customer: req.session.user.customer.id }, (err, plants) => {
    if (err) console.log(err);
    Silo
      .find({ plant: { $in: plants.map(o => o._id) } })
      .select('-_id -__v').exec((err, silos) => {
      if (err) console.log(err);
      res.send({ silos: silos });
    });
  })
});


module.exports = router;