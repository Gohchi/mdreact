const User = require('../models/user');

module.exports = api => {
  api.get('/session', (req, res) => {
    if (req.session.user && req.cookies.user_sid) {
      res.send({ user: req.session.user });
    } else {
      res.send({ isNotLoggedIn: true });
    }
  });

  api.get('/logout', (req, res) => {
    delete req.session.user;
    res.send({ isNotLoggedIn: true });
  });
  api.post('/login', (req, res) => {
    let password = req.body.password;
    let email = req.body.email;

    req.checkBody('email', 'El email es requerido').notEmpty();
    req.checkBody('email', 'Ingrese un email válido').isEmail();
    req.checkBody('password', 'La contraseña es requerida').notEmpty();

    const errors = req.validationErrors();
    if(errors) {
      req.session.errors = errors;
      // res.status(200).send(errors[0].msg); //{ errors: errors });
      res.status(401).send({ message: errors[0].msg, isNotLoggedIn: true });
    } else {
      
      User.find(
        {
          email: email,
          password: password
        }).populate('customer')
        .exec(
        (err, users) => {
        if (err) console.log(err);
        if(users.length) {
          let user = users[0];
          req.session.success = true;
          //temp
          req.session.user = {
            name: user.name,
            lastName: user.lastName,
            email: user.email,
            fullName: `${user.name} ${user.lastName}`,
            customer: {
              id: user.customer._id,
              name: user.customer.name,
              icon: "none"
            }
          };
          res.send({ user: req.session.user });
        } else {
          req.session.errors = 'you shall not pass';
          res.status(401).send({ message: 'Email o contraseña incorrecta', isNotLoggedIn: true });
        }
      });
    }
  });

}